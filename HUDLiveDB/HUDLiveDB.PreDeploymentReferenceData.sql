﻿/*
 Pre-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be executed before the build script.	
 Use SQLCMD syntax to include a file in the pre-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the pre-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--Bug 4617
IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'FileName92464') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column FileName92464 nvarchar(260) null
END
GO

IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'FileName92117') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column FileName92117 nvarchar(260) null
END
GO

IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'ScopeCertificationFileName') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column ScopeCertificationFileName nvarchar(260) null
END
GO

IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'FinalInspectionReport') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column FinalInspectionReport nvarchar(260) null
END
GO
IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'DefaultRequestExtensionFileName') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column DefaultRequestExtensionFileName nvarchar(260) null
END
GO
IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'ContractFileName') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column ContractFileName nvarchar(260) null
END
GO
IF (SELECT CHaracter_Maximum_Length 
	FROM INFORMATION_SCHEMA.COLUMNS
	WHERE 
     TABLE_NAME = 'NonCriticalRepairsRequests' AND 
     COLUMN_NAME = 'FileNamePCNA') < 260
BEGIN
alter table NonCriticalRepairsRequests alter column FileNamePCNA nvarchar(260) null
END
GO