﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Alter
       [HUDHealthCareDevUser] (User)
       [dbo].[Prod_FHANumberRequest] (Table)
       [dbo].[fn_HCP_GetAEEmailbyId] (Function)
       [dbo].[fn_HCP_GetReceivedCount] (Function)
       [dbo].[fn_HCP_GetTotalExpectedCount] (Function)
       [dbo].[fn_HCP_GetMissingQuarters] (Function)
       [dbo].[usp_HCP_Prod_GetGeneralInfoDetailsForSharePointScreen] (Procedure)
       [dbo].[usp_HCP_GetLenderPAMReportDetail] (Procedure)
       [dbo].[usp_HCP_GetPAMReportDetail] (Procedure)
       [dbo].[usp_HCP_GetTaskFileCommentsHistory] (Procedure)
       [dbo].[usp_HCP_Prod_AssignedTo] (Procedure)
       [dbo].[usp_HCP_Prod_GetFolderInfoByKey] (Procedure)
       [dbo].[usp_HCP_Prod_GetLenderUpload] (Procedure)
       [dbo].[usp_HCP_Prod_GetOpaHistory_FileList] (Procedure)
       [dbo].[usp_HCP_Prod_GetOpaHistory_ReviewersList] (Procedure)
       [dbo].[usp_HCP_Prod_GetRAIEdit] (Procedure)
       [dbo].[usp_HCP_Prod_GetSubFolder] (Procedure)
       [dbo].[usp_HCP_Prod_GetWPUpload] (Procedure)
       [dbo].[usp_HCP_Prod_InsertSubFolder] (Procedure)
     Create
       [dbo].[HUD_PamReportFilters] (Table)
       [dbo].[HUDSignature] (Table)
       [dbo].[Prod_LoanCommittee] (Table)
       [dbo].[usp_HCP_EmailIssueCorrection] (Procedure)
       [dbo].[usp_HCP_GetAllInternalSpecialOptionUsers] (Procedure)
       [dbo].[usp_HCP_GetProductionWLMUsers] (Procedure)
       [dbo].[usp_HCP_GetPropertyInfo] (Procedure)
       [dbo].[usp_HCP_Prod_GetAllProdData] (Procedure)
       [dbo].[usp_HCP_Prod_GetProdLenderUploadForAmendments] (Procedure)

** Supporting actions
     Refresh
       [dbo].[USP_GetSelectedFhasForInspectedContractor] (Procedure)
       [dbo].[usp_HCP_Delete_Script_For_Automation] (Procedure)
       [dbo].[usp_HCP_Prod_GetAgingFHAReport] (Procedure)
       [dbo].[usp_HCP_Prod_GetApplicationDetailsForSharePointScreen] (Procedure)
       [dbo].[usp_HCP_Prod_GetFhaRequestByTaskInstanceId] (Procedure)
       [dbo].[usp_HCP_Prod_GetFhaSubmittedLenders] (Procedure)
       [dbo].[usp_HCP_Prod_GetReadyFhasForInspectionContractor] (Procedure)
       [dbo].[usp_HCP_Prod_GetSubmittedFhaRequests] (Procedure)
       [dbo].[usp_HCP_Get_MissingProject_ManagementReport] (Procedure)
       [dbo].[usp_HCP_GetProjectDetailReport_HighLevel] (Procedure)
       [dbo].[usp_HCP_Get_MissingProject_ManagementReport_HighLevel] (Procedure)

If this deployment is executed, changes to [HUDHealthCareDevUser] might introduce run-time errors in [db_owner].
If this deployment is executed, changes to [HUDHealthCareDevUser] might introduce run-time errors in [db_datareader].
If this deployment is executed, changes to [HUDHealthCareDevUser] might introduce run-time errors in [db_datawriter].

