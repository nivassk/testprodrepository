﻿CREATE FUNCTION [dbo].[fn_HCP_GetTotalExpectedCount]
(
   @Year INT,
   @WLMId INT,
   @AEId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @PeriodEnding DATETIME
	DECLARE @FHANumber VARCHAR(100)
	DECLARE @Count INT
	DECLARE @SumOfCount INT
	SET @SumOfCount = 0
	DECLARE @StartDate DATETIME
	DECLARE @EndDate DATETIME
	DECLARE @YearDiff INT
	DECLARE @YearCount INT
	DECLARE @CurrYear BIT
	IF(@Year = YEAR(GETDATE()))
		BEGIN
			SET @StartDate = DATEADD(YYYY, DATEDIFF(YYYY,0,getdate()), 0) 
			SET @EndDate = GETDATE() - 1
		END
	ELSE
		BEGIN
			SET @YearDiff = CONVERT(INT,(YEAR(GETDATE()) - @Year))
			SET @StartDate = DATEADD(yy, DATEDIFF(yy,0,getdate()) - @YearDiff, 0) 
			SET @EndDate = DATEADD(YYYY, DATEDIFF(YYYY,0,getdate()) - @YearDiff + 1, -1)
		END
			
	IF(@Year = YEAR(GETDATE()))
		SET @CurrYear = 1
	ELSE
		SET @CurrYear = 0

	DECLARE @ExpectedCount_cursor CURSOR   
	IF @WLMId > 0
	 BEGIN		
		IF @CurrYear = 1
		BEGIN
			SET @ExpectedCount_cursor = CURSOR FOR
			(SELECT DISTINCT LDL.FHANumber FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL
		   INNER JOIN [$(DatabaseName)].dbo.ProjectInfo PF ON LDL.FHANumber = PF.FHANumber
		   WHERE LDL.FHANumber IN 
		   (SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber
		    WHERE (FHA_EndDate is null or Year(FHA_EndDate) > (GETDATE()-1))
				and (Year(FHA_StartDate) <= @Year))
			AND MonthsInPeriod IN (3,6,9,12)   
		   AND PF.HUD_WorkLoad_Manager_ID = @WLMId )
		END
		ELSE IF @CurrYear = 0
		BEGIN
			SET @ExpectedCount_cursor = CURSOR FOR
			(SELECT DISTINCT LDL.FHANumber FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL
		   INNER JOIN [$(DatabaseName)].dbo.ProjectInfo PF ON LDL.FHANumber = PF.FHANumber
		   WHERE LDL.FHANumber IN 
		   (SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber
		    WHERE (FHA_EndDate is null or Year(FHA_EndDate) > @Year)
				and (Year(FHA_StartDate) <= @Year))
			AND MonthsInPeriod IN (3,6,9,12)   
		   AND PF.HUD_WorkLoad_Manager_ID = @WLMId )
		END   
	 END
	ELSE IF @AEId > 0
	 BEGIN
		SET @ExpectedCount_cursor = CURSOR FOR
		   SELECT DISTINCT LDL.FHANumber FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live LDL
		   INNER JOIN [$(DatabaseName)].dbo.ProjectInfo PF ON LDL.FHANumber = PF.FHANumber
		   WHERE LDL.FHANumber IN 
		   (SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate is null) AND MonthsInPeriod IN (3,6,9,12)   
		   AND PF.HUD_Project_Manager_ID = @AEId
	 END
	ELSE 
	 BEGIN 
		SET @ExpectedCount_cursor = CURSOR FOR
		SELECT DISTINCT FHANumber FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber IN 
		(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate is null) 
		AND MonthsInPeriod IN (3,6,9,12)  --AND YEAR(PeriodEnding) = @Year
	 END
	 
	OPEN @ExpectedCount_cursor   
	FETCH NEXT FROM @ExpectedCount_cursor INTO @FHANumber   

	WHILE @@FETCH_STATUS = 0   
	BEGIN  
		SET @Count = 0
		SET @PeriodEnding = (SELECT MAX(PeriodEnding) FROM [$(DatabaseName)].dbo.Lender_DataUpload_Live WHERE FHANumber  IN 
								(SELECT DISTINCT(FHANumber) FROM [$(DatabaseName)].dbo.Lender_FHANumber WHERE FHA_EndDate IS NULL) AND 
									MonthsInPeriod IN (3,6,9,12) AND FHANumber = @FHANumber)
			IF(YEAR(@PeriodEnding) <> @Year)
			BEGIN
				SET @YearCount = CONVERT(INT,(@Year - YEAR(@PeriodEnding)))
				SET @PeriodEnding = DATEADD(YY, @YearCount,@PeriodEnding) 
			END
			ELSE IF(YEAR(@PeriodEnding) = YEAR(GETDATE()))
			BEGIN
				IF(YEAR(@PeriodEnding) <> @Year)
				BEGIN
					SET @YearCount = CONVERT(INT,(@Year - YEAR(@PeriodEnding)))
					SET @PeriodEnding = DATEADD(YY, @YearCount,@PeriodEnding)
				END
			END			
			IF DATEDIFF(DAY,@StartDate,DATEADD(month, -9, @PeriodEnding)) >= 0
			BEGIN 
				IF @CurrYear = 0
					SET @Count = @Count + 1
			END
			IF DATEDIFF(DAY,@StartDate,DATEADD(month, -6, @PeriodEnding)) >= 0
			BEGIN 
				IF @CurrYear = 0
					SET @Count = @Count + 1
			END
			IF DATEDIFF(DAY,@StartDate,DATEADD(month, -3, @PeriodEnding)) >= 0
			BEGIN 
				IF @CurrYear = 0
					SET @Count = @Count + 1
			END
			IF DATEDIFF(DAY,@PeriodEnding, @EndDate) >= 0
			BEGIN 
				SET @Count = @Count + 1
			END
			IF DATEDIFF(DAY,DATEADD(month, 3, @PeriodEnding),@EndDate) >= 0
			BEGIN 
				SET @Count = @Count + 1
			END
			IF DATEDIFF(DAY,DATEADD(month, 6, @PeriodEnding),@EndDate) >= 0
			BEGIN 
				SET @Count = @Count + 1
			END
			IF DATEDIFF(DAY,DATEADD(month, 9, @PeriodEnding),@EndDate) >= 0
			BEGIN 
				SET @Count = @Count + 1
			END
			--prINt @FHANumber+N' '+ CONVERT(char(30), @PeriodEnding) + N' '+CONVERT(char(30), @Count)
			SET @SumOfCount = @SumOfCount + @Count
			FETCH NEXT FROM @ExpectedCount_cursor INTO @FHANumber   
		END  
		CLOSE @ExpectedCount_cursor   
		DEALLOCATE @ExpectedCount_cursor

    RETURN @SumOfCount
END