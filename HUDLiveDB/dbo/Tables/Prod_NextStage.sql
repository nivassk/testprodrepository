﻿
CREATE TABLE [dbo].[Prod_NextStage](
	[NextStgId] [uniqueidentifier] NOT NULL,
	[CompletedPgTypeId] [int] NULL,
	[NextPgTypeId] [int] NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NULL,
	[FhaNumber] [nvarchar](max) NULL,
	[CompletedPgTaskInstanceId] [uniqueidentifier] NULL,
	[NextPgTaskInstanceId] [uniqueidentifier] NULL,
	[IsNew] [bit] NULL,
 CONSTRAINT [PK_Prod_NextStage] PRIMARY KEY CLUSTERED 
(
	[NextStgId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

