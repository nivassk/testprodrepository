﻿
CREATE TABLE [dbo].[HCP_User_LoginTime](
	[UserLoginID] [uniqueidentifier] NOT NULL,
	[Login_Time] [datetime] NOT NULL,
	[Logout_Time] [datetime] NULL,
	[RoleName] [nvarchar](50) NOT NULL,
	[Session_Id] [nvarchar](max) NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[User_id] [int] NULL,
 CONSTRAINT [PK_HCP_User_UserLoginID] PRIMARY KEY CLUSTERED 
(
	[UserLoginID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

