﻿
CREATE TABLE [dbo].[User_SecurityQuestion](
	[User_SecurityQuestionID] [int] IDENTITY(1,1) NOT NULL,
	[User_ID] [int] NOT NULL,
	[ModifiedOn] [datetime] NOT NULL,
	[ModifiedBy] [int] NOT NULL,
	[Deleted_Ind] [bit] NULL,
	[Answer] [nvarchar](128) NULL,
	[SecurityQuestionID] [int] NOT NULL,
 CONSTRAINT [PK_User_SecurityQuestionID] PRIMARY KEY CLUSTERED 
(
	[User_SecurityQuestionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[User_SecurityQuestion]  WITH CHECK ADD  CONSTRAINT [FK_User_SecurityQuestion_HCP_Authentication] FOREIGN KEY([User_ID])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[User_SecurityQuestion] CHECK CONSTRAINT [FK_User_SecurityQuestion_HCP_Authentication]
GO

ALTER TABLE [dbo].[User_SecurityQuestion]  WITH CHECK ADD  CONSTRAINT [FK_User_SecurityQuestion_SecurityQuestion] FOREIGN KEY([SecurityQuestionID])
REFERENCES [dbo].[SecurityQuestion] ([SecurityQuestionID])
GO

ALTER TABLE [dbo].[User_SecurityQuestion] CHECK CONSTRAINT [FK_User_SecurityQuestion_SecurityQuestion]
GO

