﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

CREATE TABLE [dbo].[Loans$](
	[Portfolio Number] [nvarchar](255) NULL,
	[Portfolio Name] [nvarchar](255) NULL,
	[Master Lease Number] [nvarchar](255) NULL,
	[property_id] [int] NULL,
	[fha_number_with_suffix] [nvarchar](255) NULL,
	[amortized_unpaid_principal_bal] [decimal](38, 0) NULL,
	[property_name_text] [nvarchar](255) NULL,
	[Account Executive] [nvarchar](255) NULL,
	[HUD Account Executive Telephone Number] [nvarchar](255) NULL,
	[HUD Account Executive Email Address] [nvarchar](255) NULL,
	[Workload Manager] [nvarchar](255) NULL,
	[HUD Workload Manager Telephone Number] [nvarchar](255) NULL,
	[HUD Workload Manager Email Address] [nvarchar](255) NULL,
	[servicing_mortgagee_id] [int] NULL,
	[servicing_mortgagee_name] [nvarchar](255) NULL,
	[servicing_mortgagee_address_1] [nvarchar](255) NULL,
	[servicing_mortgagee_address_2] [nvarchar](255) NULL,
	[servicing_mortgagee_city] [nvarchar](255) NULL,
	[servicing_mortgagee_state] [nvarchar](255) NULL,
	[servicing_mortgagee_zip] [nvarchar](255) NULL,
	[owner_organization_name] [nvarchar](255) NULL,
	[owner_address_line1] [nvarchar](255) NULL,
	[owner_address_line2] [float] NULL,
	[owner_city_name] [nvarchar](255) NULL,
	[owner_state_code] [nvarchar](255) NULL,
	[owner_zip_code] [nvarchar](255) NULL,
	[owner_main_phone_number_text] [nvarchar](255) NULL,
	[owner_email_text] [nvarchar](255) NULL,
	[owner_contact_indv_full_name] [nvarchar](255) NULL,
	[owner_contact_indv_title_text] [nvarchar](255) NULL,
	[owner_contact_address_line1] [nvarchar](255) NULL,
	[owner_contact_address_line2] [float] NULL,
	[owner_contact_city_name] [nvarchar](255) NULL,
	[owner_contact_state_code] [nvarchar](255) NULL,
	[owner_contact_zip_code] [nvarchar](255) NULL,
	[owner_contact_main_phone_num] [nvarchar](255) NULL,
	[owner_contact_email_text] [nvarchar](255) NULL,
	[mgmt_agent_org_name] [nvarchar](255) NULL,
	[mgmt_agent_address_line1] [nvarchar](255) NULL,
	[mgmt_agent_address_line2] [float] NULL,
	[mgmt_agent_city_name] [nvarchar](255) NULL,
	[mgmt_agent_state_code] [nvarchar](255) NULL,
	[mgmt_agent_zip_code] [nvarchar](255) NULL,
	[mgmt_agent_main_phone_number] [nvarchar](255) NULL,
	[mgmt_agent_email_text] [nvarchar](255) NULL,
	[mgmt_contact_full_name] [nvarchar](255) NULL,
	[mgmt_contact_indv_title_text] [float] NULL,
	[mgmt_contact_address_line1] [nvarchar](255) NULL,
	[mgmt_contact_address_line2] [float] NULL,
	[mgmt_contact_city_name] [nvarchar](255) NULL,
	[mgmt_contact_state_code] [nvarchar](255) NULL,
	[mgmt_contact_zip_code] [nvarchar](255) NULL,
	[mgmt_contact_main_phn_nbr] [nvarchar](255) NULL,
	[mgmt_contact_email_text] [float] NULL
) ON [PRIMARY]
