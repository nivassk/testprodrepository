﻿
CREATE TABLE [dbo].[NonCriticalRepairsRequests](
	[NonCriticalRepairsRequestID] [uniqueidentifier] NOT NULL,
	[PropertyId] [int] NOT NULL,
	[LenderId] [int] NOT NULL,
	[FhaNumber] [nvarchar](15) NOT NULL,
	[SubmittedDate] [datetime] NOT NULL,
	[ApprovedDate] [datetime] NULL,
	[PaymentRequested] [decimal](10, 2) NOT NULL,
	[IsFinalDraw] [bit] NULL,
	[IsScopeOfWorkChanged] [bit] NULL,
	[IsAdvance] [bit] NULL,
	[FileName92464] [nvarchar](260) NULL,
	[FileName92117] [nvarchar](260) NULL,
	[ScopeCertificationFileName] [nvarchar](260) NULL,
	--[ClearTitleFileName] [nvarchar](100) NULL,
	FinalInspectionReport [nvarchar](260) NULL,
	[DefaultRequestExtensionFileName] [nvarchar](260) NULL,
	[ContractFileName] [nvarchar](260) NULL,
	[NumberDraw] [int] NOT NULL,
	[CanProvidePhotosInvoices] [bit] NULL,
	[RequestStatus] [int] NULL,
	[ApprovedAmount] [decimal](10, 2) NULL,
	[Reason] [nvarchar](500) NULL,
	[TaskId] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedOn] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[ServicerRemarks] [nvarchar](500) NULL,
	[IsLenderDelegate] [bit] NULL,
	[FileNamePCNA] [nvarchar](260) NULL,
 CONSTRAINT [PK_NonCriticalRepairRequests] PRIMARY KEY CLUSTERED 
(
	[NonCriticalRepairsRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[NonCriticalRepairsRequests]  WITH CHECK ADD  CONSTRAINT [fk_NonCriticalRepairRequests_HCP_Authentication_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[NonCriticalRepairsRequests] CHECK CONSTRAINT [fk_NonCriticalRepairRequests_HCP_Authentication_CreatedBy]
GO

ALTER TABLE [dbo].[NonCriticalRepairsRequests]  WITH CHECK ADD  CONSTRAINT [fk_NonCriticalRepairRequests_HCP_Authentication_ModifiedBy] FOREIGN KEY([ModifiedBy])
REFERENCES [dbo].[HCP_Authentication] ([UserID])
GO

ALTER TABLE [dbo].[NonCriticalRepairsRequests] CHECK CONSTRAINT [fk_NonCriticalRepairRequests_HCP_Authentication_ModifiedBy]
GO

