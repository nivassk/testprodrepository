﻿
CREATE TABLE [dbo].[HCP_EmailType](
	[EmailTypeId] [tinyint] NOT NULL,
	[EmailTypeCd] [nvarchar](10) NOT NULL,
	[EmailTypeDescription] [nchar](50) NOT NULL,
	[Deleted_ind] [bit] NULL,
 CONSTRAINT [PK_HCP_EmailType] PRIMARY KEY CLUSTERED 
(
	[EmailTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

