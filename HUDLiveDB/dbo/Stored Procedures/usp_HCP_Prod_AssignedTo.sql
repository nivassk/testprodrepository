﻿CREATE PROCEDURE [dbo].[usp_HCP_Prod_AssignedTo]
(
@PagetypeId int,
@FHANumber varchar(10) = null
)
AS

BEGIN
SELECT  au.UserID,au.UserName
        FROM [$(TaskDB)].[dbo].[Prod_TaskXref] tx join [$(DatabaseName)].[dbo].[HCP_Authentication] au on tx.ModifiedBy=au.UserID
  where ViewId=1 and TaskInstanceId=(
  select top 1 CompletedPgTaskInstanceId from [$(DatabaseName)].[dbo].[Prod_NextStage] where FhaNumber=@FHANumber and NextPgTypeId=@PagetypeId )

--select UserID,UserName from [HCP_Live_db_Prod].[dbo].[HCP_Authentication] where UserID=(
--  select  top 1 modifiedby from [HCP_Live_db_Prod].[dbo].[Prod_NextStage] where FhaNumber=@FHANumber and NextPgTypeId=@PagetypeId)
			  		
end