﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetProdOPAByFHA]
(@FhaNumber varchar(20))
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    

	select * from OPAForm o
	join [$(TaskDB)].dbo.Task t
	on o.TaskInstanceId = t.TaskInstanceId
	where o.FhaNumber = @FhaNumber and PageTypeId > 4
	and o.RequestStatus in (1,6,7)
END

GO

