﻿
CREATE PROCEDURE [dbo].[usp_HCP_Prod_GetFhaSubmittedLenders]

AS
select distinct lender.LenderID,lender.Lender_Name  from Prod_FHANumberRequest fhaRequest
join [$(TaskDB)].dbo.Task task on task.TaskInstanceId= fhaRequest.TaskinstanceId
join [$(DatabaseName)].dbo.LenderInfo lender on fhaRequest.LenderId = lender.LenderID

GO

