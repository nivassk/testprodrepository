﻿
CREATE PROCEDURE [dbo].[usp_HCP_GetOpaHistoryForRequestAdditionalInfo]
(
@TaskInstanceId uniqueidentifier
)
AS
BEGIN


      
       select 'Request Addtional Information' as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
			WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
			WHEn r.RoleName = 'LenderAccountManager' then 'LAM'
			WHEN r.RoleName = 'OperatorAccountRepresentative' then 'OAR'
			WHEN r.RoleName = 'InspectionContractor' then 'IC'
			
			WHEN r.RoleName = 'AccountExecutive' then 'AE' end as userRole,
			null as comment,
			[FileName] as [fileName],
			(select FileSize from [$(TaskDB)].dbo.TaskFile where TaskFileId = ChildTaskFileId) as fileSize,
			(select UploadTime from [$(TaskDB)].dbo.TaskFile where TaskFileId = ChildTaskFileId) as uploadDate,
			null as submitDate,
			fileId,
			((select FileId from [$(TaskDB)].dbo.TaskFile where TaskFileId = ChildTaskFileId)) as childFileId,
			case when t.TaskInstanceId is null then CONVERT(BIT,1)
				 when t.TaskStepId = 16 and t.StartTime < tf.UploadTime then CONVERT(BIT,1)  
				 else CONVERT(BIT,0) end as editable,
			uploadTime as actionDate,
			(select Comments from [$(TaskDB)].dbo.NewFileRequest nf where ChildTaskInstanceId = @TaskInstanceId) as nfrComment,
			case when (fh.ChildTaskFileId is null) then CONVERT(BIT,0) else CONVERT(BIT,1) end as isFileHistory,
			case when (rc.Comment <> '' or rc.Comment <> null) then CONVERT(BIT,1) else CONVERT(BIT,0) end as isFileComment,
			case when rs.Status = 5 then CONVERT(BIT,1) else CONVERT(BIT,0) end as isApprove,
			((select FileName from [$(TaskDB)].dbo.TaskFile where TaskFileId = ChildTaskFileId)) as childFileName
	from [$(TaskDB)].dbo.TaskFile tf
	inner join [$(DatabaseName)].dbo.HCP_Authentication a on a.UserName = (select top 1 AssignedBy from [$(TaskDB)].dbo.Task where TaskInstanceId = @TaskInstanceId order by SequenceId desc)
	inner join [$(DatabaseName)].dbo.webpages_UsersInRoles l on l.UserId = a.userid
	inner join [$(DatabaseName)].dbo.webpages_Roles r on r.roleid = l.roleid
	inner join [$(TaskDB)].dbo.RequestAdditionalInfoFiles rf
	on tf.TaskFileId = rf.TaskFileId
	left join (	select ParentTaskFileId,ChildTaskInstanceId, max(ChildTaskFileId) as ChildTaskFileId,max(CreatedOn) as Maxdate from [$(TaskDB)].dbo.TaskFileHistory
	group by ParentTaskFileId,ChildTaskInstanceId) fh
	on rf.ChildTaskInstanceId = fh.ChildTaskInstanceId
	and rf.TaskFileId = fh.ParentTaskFileId
	left join
		(select top 1 * from [$(TaskDB)].dbo.Task where TaskInstanceId = @TaskInstanceId order by SequenceId desc) t 
		on t.TaskInstanceId = rf.ChildTaskInstanceId
	inner join [$(TaskDB)].dbo.ParentChildTask pt on pt.ChildTaskInstanceId = t.TaskInstanceId	
	left join [$(TaskDB)].dbo.ReviewFileComment rc on tf.TaskFileId = rc.FileTaskId	
	left join [$(TaskDB)].dbo.ReviewFileStatus rs on tf.TaskFileId = rs.TaskFileId
	where t.TaskInstanceId = @TaskInstanceId 
	and r.RoleName in ('LenderAccountRepresentative','BackupAccountManager','LenderAccountManager','OperatorAccountRepresentative','AccountExecutive','InspectionContractor')

Union

	select 'New File'  as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		 CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 	WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole,	
		  null as comment,
		  tf.FileName as [fileName],
			FileSize as fileSize,
			tf.UploadTime as uploadDate,
			UploadTime as submitDate,
			tf.FileId as fileId,
			null as childFileId,
			CONVERT(BIT,0) as editable,
			UploadTime as actionDate,
			null as nfrComment,
			CONVERT(BIT,0) as isFileHistory,
			CONVERT(BIT,0) as isFileComment,
			CONVERT(BIT,0) as isApprove,
			tf.FileName as childFileName
	from  [$(TaskDB)].dbo.TaskFile tf
	join [$(TaskDB)].dbo.ChildTaskNewFiles ct
	on tf.TaskFileId = ct.taskfileinstanceId
	inner join [$(DatabaseName)].dbo.HCP_Authentication a on a.UserID = tf.CreatedBy
	left join [$(DatabaseName)].dbo.webpages_UsersInRoles l on l.UserId = a.userid 
	left join [$(DatabaseName)].dbo.webpages_Roles r on r.roleid = l.roleid
	where ct.ChildTaskInstanceId = @TaskInstanceId

Union 

select 'Request Additional Submit'  as actionTaken,
		(A.FirstName + ' ' + A.LastName) as name,
		 CASE WHEN r.RoleName = 'LenderAccountRepresentative' then 'LAR'
                     WHEN r.RoleName = 'BackupAccountManager' then 'BAM'
					 	WHEN r.RoleName = 'InspectionContractor' then 'IC'
                     WHEn r.RoleName = 'LenderAccountManager' then 'LAM' end as userRole,	
		  Notes as comment,
		  '' as [fileName],
			null as fileSize,
			null as uploadDate,
			starttime as submitDate,
			null as fileId,
			null as childFileId,
			CONVERT(BIT,0) as editable,
			starttime as actionDate,
			null as nfrComment,
			CONVERT(BIT,0) as isFileHistory,
			CONVERT(BIT,0) as isFileComment,
			CONVERT(BIT,0) as isApprove,
			null as childFileName
	from [$(TaskDB)].dbo.Task t
	inner join [$(DatabaseName)].dbo.HCP_Authentication a on a.UserName = t.AssignedBy
	left join [$(DatabaseName)].dbo.webpages_UsersInRoles l on l.UserId = a.userid 
	left join [$(DatabaseName)].dbo.webpages_Roles r on r.roleid = l.roleid
	where  t.TaskInstanceId = @TaskInstanceId
	and TaskStepId = 15
end

GO

