﻿
CREATE PROCEDURE [dbo].[usp_HCP_Soft_Delete_User]
(
@UserId int,
@Inactive bit
)
AS
Begin try
	Begin transaction
		
		Declare @Username varchar(max)
		Set @Username = (Select UserName from HCP_Authentication where UserID = @UserId)
		/* delete user accounts only - start */
		update User_SecurityQuestion
		set Deleted_Ind = @Inactive
		where User_ID = @UserId

		update User_Lender
		set Deleted_Ind = @Inactive
		where User_ID = @UserId

		update User_WLM_PM
		set Deleted_Ind = @Inactive 
		where UserID = @UserId

		update HCP_Authentication
		set Deleted_Ind = @Inactive
		where UserID = @UserId

		update Address 
		set Deleted_Ind = @Inactive
		where Email = @Username
		/* delete user accounts only - end */
	Commit transaction
	
End try
Begin catch
	
	Rollback transaction
End catch











GO

