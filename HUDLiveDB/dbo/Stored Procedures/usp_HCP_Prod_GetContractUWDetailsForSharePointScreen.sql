﻿
-- =============================================
-- Author:		<Rashmi Ganapathy>
-- Create date: <03/22/2017>
-- Description:	<Get Contract Underwriter details for Sharepoint screen - Miscellaneous information>
-- =============================================
create PROCEDURE [dbo].[usp_HCP_Prod_GetContractUWDetailsForSharePointScreen]
(@TaskInstanceId uniqueidentifier)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select 
	Case When (tx.AssignedTo > 0 or tx.AssignedTo <> null) then Convert(bit,1) else Convert(bit,0) end as IsUnderwritingContractorAssigned,
	Concat(au.FirstName,' ',au.LastName) as UWContractorName,
	au.Username as UWContractorEmailAddress,
	ad.PhonePrimary as UWContractorPhoneNumber,
	tk.FhaNumber as FHANumber,
	tk.TaskInstanceId as TaskinstanceId
	from [$(TaskDB)].dbo.Prod_TaskXref tx
	join [$(TaskDB)].dbo.Task tk
	on tx.Taskinstanceid = tk.taskinstanceid
	join  HCP_Authentication au
	on au.userid = tx.AssignedTo
	join Address ad
	on ad.addressid = au.addressid
	where ViewId = 6 and tk.TaskInstanceId = @TaskInstanceId
END


GO

