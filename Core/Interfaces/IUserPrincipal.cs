﻿namespace Core.Interfaces
{
    public interface IUserPrincipal
    {
        int? LenderId { get; }
        int? ServicerId { get; }
        string Timezone { get; }
        int UserId { get; }
        string UserName { get; }
        string UserRole { get; }
    }
}
