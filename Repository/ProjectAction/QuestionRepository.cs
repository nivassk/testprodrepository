﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.ProjectAction;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces.ProjectAction;

namespace Repository.ProjectAction
{
    public class QuestionRepository :BaseRepository<CheckListQuestion>, IQuestionRepository
    {

        public QuestionRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }

        public QuestionRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }
        
        public void AddQuestion(QuestionViewModel questionViewModel)
        {
            var checkListQuestion = Mapper.Map<CheckListQuestion>(questionViewModel);
            checkListQuestion.CreatedBy = UserPrincipal.Current.UserId;
            checkListQuestion.CreatedOn = DateTime.UtcNow;
            this.InsertNew(checkListQuestion);
           // return checkListQuestion.NonCriticalRequestExtensionId;
        }

        private Dictionary<int?, string> GetAllSections(int projectActionTypeId)
        {
            var context = (HCP_live)this.Context;

            var results =
                this.GetAll()
                    .Where(p => p.ProjectActionId == projectActionTypeId).OrderBy(p=>p.DisplayOrderId)
                    .GroupBy(p => new {p.SectionId,p.SectionName})
                    .Select(p=> new  {p.Key.SectionId,p.Key.SectionName}).ToList();
            var resultList = new Dictionary<int?, string>();
            foreach (var item in results)
            {
                if (item.SectionId != null)
                {
                    resultList.Add(item.SectionId, item.SectionName);    
                }
            }
            return resultList;

        }
        public IDictionary<string, IList<QuestionViewModel>> GetAllQuestionsBySections(int projectActionTypeId)
        {
            var context = (HCP_live)this.Context;

            
      
            var result = (from n in context.CheckListQuestion
                          where n.ProjectActionId == projectActionTypeId
                          orderby n.DisplayOrderId
                          select n
                             );
            var allChecklistQuestions = Mapper.Map<List<CheckListQuestion>, List<QuestionViewModel>>(result.ToList());
            var resultList = GetAllSections(projectActionTypeId);
            IList<QuestionViewModel> questionViewList;
            var checklistWithSection = new Dictionary<string, IList<QuestionViewModel>>();
            foreach (var sectionId in resultList.Keys)
            {
                questionViewList = new List<QuestionViewModel>();
                foreach (var checklistQuestion in allChecklistQuestions)
                {
                    if (sectionId == checklistQuestion.SectionId)
                    {
                        questionViewList.Add(checklistQuestion);
                    }
                }
                checklistWithSection.Add(resultList[sectionId], questionViewList);
            }

            return checklistWithSection;
        }

        public IList<QuestionViewModel> GetAllQuestions(int projectActionTypeId)
        {
            var context = (HCP_live)this.Context;



            var result = (from n in context.CheckListQuestion
                          where n.ProjectActionId == projectActionTypeId
                          orderby n.DisplayOrderId
                          select n
                             );
            var allChecklistQuestions = Mapper.Map<List<CheckListQuestion>, List<QuestionViewModel>>(result.ToList());
            return allChecklistQuestions;
        } 
        public void UpdateQuestion(QuestionViewModel questionViewModel)
        {
            var context = (HCP_live)this.Context;
            var checkListQuestion = (from n in context.CheckListQuestion
                                               where n.CheckListId  == questionViewModel.CheckListId
                                               select n).FirstOrDefault();
            checkListQuestion.QuestionId = questionViewModel.QuestionId;
            checkListQuestion.Question = questionViewModel.Question;
            checkListQuestion.ParentQuestionId = questionViewModel.ParentQuestionId;
            checkListQuestion.Level = questionViewModel.Level;
            checkListQuestion.EndDate = questionViewModel.EndDate;
            checkListQuestion.StartDate = questionViewModel.StartDate;
            checkListQuestion.IsAttachmentRequired = questionViewModel.IsAttachmentRequired;
            checkListQuestion.DisplayOrderId = questionViewModel.DisplayOrderId;
            checkListQuestion.ModifiedBy = questionViewModel.ModifiedBy;
            checkListQuestion.ModifiedOn = questionViewModel.ModifiedOn;
            checkListQuestion.SectionId = questionViewModel.SectionId;
            checkListQuestion.SectionName = questionViewModel.SectionName;
            checkListQuestion.Shortnames = questionViewModel.Shortnames;
            checkListQuestion.IsNARequired = questionViewModel.IsNARequired;
            checkListQuestion.IsNAForSection = questionViewModel.IsNAForSection;

            this.Update(checkListQuestion);
        }

        public QuestionViewModel GetQuestion(Guid checkListId)
        {
            var context = (HCP_live)this.Context;
            var checkListQuestion = (from n in context.CheckListQuestion
                                     where n.CheckListId == checkListId
                                     select n).FirstOrDefault();

            return Mapper.Map<CheckListQuestion, QuestionViewModel>(checkListQuestion);
        }
    }
}
