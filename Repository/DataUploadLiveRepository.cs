﻿using System.Globalization;
using AutoMapper;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity.SqlServer;
using EntityObject.Entities.HCP_live;
using Repository;
using Repository.Interfaces;

namespace HUDHealthcarePortal.Repository
{
    public class DataUploadLiveRepository : BaseRepository<Lender_DataUpload_Live>, IDataUploadLiveRepository
    {
        public DataUploadLiveRepository()
            : base(new UnitOfWork(DBSource.Live))
        {
        }
        public DataUploadLiveRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<Lender_DataUpload_Live> DataToJoin
        {
            get { return DbQueryRoot; }
        }

        public IEnumerable<ExcelUploadView_Model> GetUploadedData(int monthsInPeriod, int year)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in UpdateLenderPropertyInfo, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetRiskSummaryReportByQuarter_Result>(
                    "usp_HCP_GetRiskSummaryReportByQuarter",
                    new {MonthsInPeriod = monthsInPeriod, Year = year});
            return  Mapper.Map<IEnumerable<usp_HCP_GetRiskSummaryReportByQuarter_Result>, IEnumerable<ExcelUploadView_Model>>(
                results);
        }

        public IEnumerable<DerivedUploadData> GetUploadedDataByScoreRange(decimal minVal, decimal maxVal)
        {
            var uploadData = this.Find(p => p.ScoreTotal.HasValue && p.ScoreTotal.Value >= minVal && p.ScoreTotal.Value < maxVal);
            return Mapper.Map<IEnumerable<Lender_DataUpload_Live>, IEnumerable<DerivedUploadData>>(uploadData);
        }

        public PaginateSortModel<DerivedUploadData> GetUploadedDataByScoreRangeForProjectReport(decimal minVal, decimal maxVal)
        {
            var uploadData = this.FindWithPageSort(p => p.ScoreTotal.HasValue && p.ScoreTotal.Value >= minVal && p.ScoreTotal.Value < maxVal, "ProjectName", SqlOrderByDirecton.ASC, 10, 1);
            return Mapper.Map<PaginateSortModel<Lender_DataUpload_Live>, PaginateSortModel<DerivedUploadData>>(uploadData);
        }

        public IEnumerable<DerivedUploadData> GetUploadedDataByScoreRangeAndQuarter(decimal minVal, decimal maxVal, int monthsInPeriod, int year, ILenderFhaRepository lenderFhaRepository)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in UpdateLenderPropertyInfo, please pass in correct context in unit of work.");
            var results = context.Database.SqlQuerySimple<usp_HCP_GetRiskSummaryReportDetail_Result>(
                    "usp_HCP_GetRiskSummaryReportDetail", new
                    {
                        MonthsInPeriod = monthsInPeriod,
                        Year = year,
                        MinVal = Convert.ToInt32(Math.Ceiling(minVal)),
                        MaxVal = Convert.ToInt32(Math.Ceiling(maxVal))
                    });
            var derivedModelList =
                Mapper.Map<IEnumerable<usp_HCP_GetRiskSummaryReportDetail_Result>, IEnumerable<DerivedUploadData>>(
                    results);
            return derivedModelList;
        }

        public IEnumerable<DerivedUploadData> GetUploadedDataByScoreRangeAndQuarterBySearch(decimal minVal, decimal maxVal, ICommentRepository commentRepo, int monthsInPeriod, int year)
        {
            var query = (from u in this.DataToJoin
                         join c in commentRepo.DataToJoin
                         on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where u.ScoreTotal.HasValue && u.ScoreTotal.Value >= minVal && u.ScoreTotal.Value < maxVal && u.PeriodEnding.Year == year && u.MonthsInPeriod == monthsInPeriod
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Live,
                             LDI_ID = u.LDI_ID,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = SqlFunctions.StringConvert((double)SqlFunctions.DatePart("yyyy", u.PeriodEnding)).Trim()
                                             + "-" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mm", u.PeriodEnding)).Trim()
                                             + "-" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("dd", u.PeriodEnding)).Trim(),
                             //UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod.HasValue ? SqlFunctions.StringConvert((double)u.MonthsInPeriod).Trim() : string.Empty,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                              DebtCoverageRatio2 = u.DebtCoverageRatio2,
                              AverageDailyRateRatio = u.AverageDailyRateRatio,
                              NOIRatio = u.NOIRatio,
                              FHAQuarter = u.FHAQuarter,
                             HasComment = x != null
                         }).Distinct();
            return query.ToList();
        }
        public PaginateSortModel<ExcelUploadView_Model> GetUploadedDataByScoreRangeWithPageSort(decimal minVal, decimal maxVal, 
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum)
        {
            var uploadData = this.FindWithPageSort(p => p.ScoreTotal.HasValue && p.ScoreTotal.Value >= minVal && p.ScoreTotal.Value < maxVal,
                strSortBy, sortOrder, pageSize, pageNum);
            return Mapper.Map<PaginateSortModel<Lender_DataUpload_Live>, PaginateSortModel<ExcelUploadView_Model>>(uploadData);
        }


        public ExcelUploadView_Model GetProjectDetailByLDI_ID(int ldiID)
        {
            var uploadData = this.Find(p => p.LDI_ID == ldiID).FirstOrDefault();
            return Mapper.Map<Lender_DataUpload_Live, ExcelUploadView_Model>(uploadData);
        }

        public void UpdateLenderPropertyInfo(int userId, int lenderId)
        {
            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in UpdateLenderPropertyInfo, please pass in correct context in unit of work.");
            context.Database.ExecuteSqlCommandSimple("usp_HCP_Verify_PropertyID_iRems", new
            {
                LenderId = lenderId,
                UserID = userId
            });
        }

        #region Derived upload data with comment flag

        public PaginateSortModel<DerivedUploadData> GetUploadedDataByScoreRangeWithPageSort(decimal minVal, decimal maxVal,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, 
            ICommentRepository commentRepo, ILenderFhaRepository lenderFhaRepository, int monthsInPeriod, int year)
        {

            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in UpdateLenderPropertyInfo, please pass in correct context in unit of work.");
            var results =
                context.Database.SqlQuerySimple<usp_HCP_GetRiskSummaryReportDetail_Result>(
                    "usp_HCP_GetRiskSummaryReportDetail", new
                    {
                        MonthsInPeriod = monthsInPeriod,
                        Year = year,
                        MinVal = Convert.ToInt32(Math.Ceiling(minVal)),
                        MaxVal = Convert.ToInt32(Math.Ceiling(maxVal))
                    });
            var derivedModelList =
                Mapper.Map<IEnumerable<usp_HCP_GetRiskSummaryReportDetail_Result>, IEnumerable<DerivedUploadData>>(
                    results);

            foreach (var item in derivedModelList)
            {
                item.DataSource = DBSource.Live;
            }
            PaginateSortModel<DerivedUploadData> model = new PaginateSortModel<DerivedUploadData>();
            model.Entities = derivedModelList.ToList();
            model.PageSize = pageSize;
            model.TotalRows = derivedModelList.ToList().Count();
            return model;
        }

        public PaginateSortModel<DerivedUploadData> GetUploadedDataByScoreRangeWithPageSort(decimal minVal, decimal maxVal,
            string strSortBy, SqlOrderByDirecton sortOrder, int pageSize, int pageNum, ICommentRepository commentRepo, DateTime endDayOfQtr)
        {
            var query = (from u in this.DataToJoin
                         join c in commentRepo.DataToJoin
                         on u.LDI_ID equals c.LDI_ID into uc
                         from x in uc.DefaultIfEmpty()
                         where u.ScoreTotal.HasValue && u.ScoreTotal.Value >= minVal && u.ScoreTotal.Value < maxVal
                         && u.PeriodEnding == endDayOfQtr
                         select new DerivedUploadData
                         {
                             DataSource = DBSource.Live,
                             LDI_ID = u.LDI_ID,
                             ProjectName = u.ProjectName,
                             ServiceName = u.ServiceName,
                             PeriodEnding = SqlFunctions.StringConvert((double)SqlFunctions.DatePart("yyyy", u.PeriodEnding)).Trim()
                                             + "-" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("mm", u.PeriodEnding)).Trim()
                                             + "-" + SqlFunctions.StringConvert((double)SqlFunctions.DatePart("dd", u.PeriodEnding)).Trim(),
                             //UnitsInFacility = u.UnitsInFacility,
                             MonthsInPeriod = u.MonthsInPeriod.HasValue ? SqlFunctions.StringConvert((double)u.MonthsInPeriod).Trim() : string.Empty,
                             FHANumber = u.FHANumber,
                             TotalRevenues = u.TotalRevenues,
                             TotalExpenses = u.TotalExpenses,
                             MortgageInsurancePremium = u.MortgageInsurancePremium,
                             FHAInsuredPrincipalInterestPayment = u.FHAInsuredPrincipalInterestPayment,
                             ActualNumberOfResidentDays = u.ActualNumberOfResidentDays,
                             //ScoreTotal = u.ScoreTotal,
                             NOIRatio = u.NOIRatio,
                             AverageDailyRateRatio = u.AverageDailyRateRatio,
                             DebtCoverageRatio2 = u.DebtCoverageRatio2,
                             FHAQuarter = u.FHAQuarter,
                             ReportingPeriod = endDayOfQtr.ToString(CultureInfo.InvariantCulture),
                             HasComment = x != null
                         }).Distinct();
            return query.SortAndPaginate(strSortBy, sortOrder, pageSize, pageNum);
        }
        #endregion
    }
}

