﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces.Production
{
   public interface IAppProcessRepository
    {
       IEnumerable<ProductionQueueLenderInfo> GetAppRequests();
       GeneralInformationViewModel GetGeneralInfoDetailsForSharepoint(Guid taskInstanceId);
       MiscellaneousInformationViewModel GetContractUWDetails(Guid taskInstanceId);
    }
}
