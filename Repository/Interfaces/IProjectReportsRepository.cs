﻿using System;
using Model;
using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Core;

namespace Repository.Interfaces
{
    public interface IProjectReportsRepository
    {
        ProjectReportModel GetProjectReportSummary(ReportLevel reportLevel, HUDRole hudRole, string userName, string wlmId, string aeId,
            string lenderId, ReportType reportType, DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        ProjectReportModel GetProjectReportDetail(string wlmId, string aeId, string lenderId, ReportType reportType,
            DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod);
        string GetWlmId(string userName);
        string GetAeId(string userName);
    }
}
