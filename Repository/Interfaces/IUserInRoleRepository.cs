﻿using System.Collections.Generic;

namespace Repository.Interfaces
{
    public interface IUserInRoleRepository
    {
        IEnumerable<KeyValuePair<int, int>> GetUsersInRoles();
        bool IsUserProductionWlm(int userId);
    }
}
