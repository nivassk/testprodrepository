﻿using System;
using System.Data.Entity;

namespace Repository.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext Context { get; }
        void Save();
    }
}
