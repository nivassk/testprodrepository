﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.Repository.Interfaces
{
    public interface IParentChildTaskRepository
    {
        void AddParentChildTask(ParentChildTaskModel model);
        bool IsParentTaskAvailable(Guid childTaskInstanceId);
        ParentChildTaskModel GetParentTask(Guid childTaskInstanceId);
    }
}
