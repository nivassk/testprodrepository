﻿using System.Collections.Generic;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Model.AssetManagement;
using System;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using Model;
using HUDHealthcarePortal.Repository.Interfaces.AssetManagement;

namespace Repository.Interfaces
{
    public interface INonCriticalRepairsRepository
    {
       Guid  SaveNonCriticalRepairsRequest(NonCriticalRepairsViewModel model, List<NonCriticalReferReasonModel> nonCriticalReferReasons);

        void UpdateNonCriticalRepairsRequest(NonCriticalRepairsViewModel model, INonCriticalProjectInfoRepository nonCriticalProjectInfoRepository, ITransactionRepository transactionRepository);

        void UpdateTaskId(NonCriticalRepairsViewModel model);

        NonCriticalRepairsViewModel GetNCRFormById(Guid ncrId);
        NonCriticalPropertyModel GetNonCriticalStatusAndDrawInfo(NonCriticalPropertyModel model, string fhanumber);
        IList<NonCriticalRulesChecklistModel> GetNonCrticalRulesCheckList(FormType formType);
        IList<NonCriticalReferReasonModel> GetNonCrticalReferReasons(Guid ncrId);
        NonCriticalPropertyModel GetNonCrticalNonCrticalTransactions(int propertyId, int lenderId, string fhaNumber, INonCriticalProjectInfoRepository nonCriticalProjectInfoRepository);

        bool GetTransactionLedgerStatus(string fhanumber);
    }
}
