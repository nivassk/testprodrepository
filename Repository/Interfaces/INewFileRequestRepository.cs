﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface INewFileRequestRepository
    {
        void SaveNewFileRequest(NewFileRequestModel model);
        bool IsNewFileRequest(Guid childTaskInstanceId);
        NewFileRequestModel GetNewFileRequest(Guid childTaskInstanceId);
    }
}
