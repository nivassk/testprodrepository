﻿using System;
using System.Collections.Generic;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Model;

namespace Repository.Interfaces
{
    public interface ILenderFiscalYearDetailsRepository : IJoinable<LenderFiscalYearDetails>
    {
        int? GetFiscalYearEndingMonth(int lenderId, string fha);
    }
}
