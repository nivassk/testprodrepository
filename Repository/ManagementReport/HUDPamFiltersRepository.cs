﻿using System;
using System.Linq;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using System.Configuration;
using HUDHealthcarePortal.Repository.Interfaces;
using Model.Production;

namespace Repository.ManagementReport
{
    public class HUDPamFiltersRepository : BaseRepository<HUD_PamReportFilters>, IHUDPamFiltersRepository
    {

        public HUDPamFiltersRepository(UnitOfWork unitOfWorkLive) : base(new UnitOfWork(DBSource.Live))
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
        }

        public string SaveHUDPamFilters(HUDPamReportFiltersModel Model)
        {
            string Status = "Fail";
            try
            {
                var context = this.Context as HCP_live;
                if (context == null)
                    throw new InvalidCastException("context is not from db live in AddFHARequest, please pass in correct context in unit of work.");

                var CheckFilter = (from a in context.HUD_PamReportFilters where a.CreatedBy == UserPrincipal.Current.UserId && a.UserRole == UserPrincipal.Current.UserRole select a).FirstOrDefault();

                if (CheckFilter != null)
                {
                    CheckFilter.ProjectName = Model.ProjectName;
                    CheckFilter.FhaNumber = Model.FHANumber;
                    CheckFilter.Appraiser = Model.Appraiser;
                    CheckFilter.CorporateCreditReview = Model.CorporateCreditReview;
                    CheckFilter.DateFrom = Model.FromDate;
                    CheckFilter.Dateto = Model.ToDate;
                    CheckFilter.Environmentalist = Model.Environmentalist;
                    CheckFilter.ExecutedDocuments = Model.ExecutedDocuments;
                    CheckFilter.Underwriter = Model.Underwriter;
                    CheckFilter.FhaNumberRequest = Model.FHANumberRequest;
                    CheckFilter.LoanType = Model.LoanType;
                    CheckFilter.MasterLeaseNumberAssignment = Model.MasterLeaseNumberAssignment;
                    CheckFilter.PortfolioNumberAssignment = Model.PortfolioNumberAssignment;
                    CheckFilter.Status = Model.Status;
                    CheckFilter.Title_Survey = Model.Title_Survey;
                    CheckFilter.ModifiedBy = UserPrincipal.Current.UserId;
                    CheckFilter.ModifiedON = DateTime.UtcNow;
                    CheckFilter.UserRole = UserPrincipal.Current.UserRole;
					CheckFilter.ProjectStage = Model.ProjectStage;
					
                    this.Update(CheckFilter);
                    context.SaveChanges();
                    Status = "Updated";
                }
                else
                {
                    var PamFilters = Mapper.Map<HUDPamReportFiltersModel, HUD_PamReportFilters>(Model);
                    PamFilters.CreatedBy = UserPrincipal.Current.UserId;
                    PamFilters.CreatedON = DateTime.UtcNow;
                    PamFilters.ModifiedON = DateTime.UtcNow;
                    PamFilters.UserRole = UserPrincipal.Current.UserRole;
                    this.InsertNew(PamFilters);
                    context.SaveChanges();
                    Status = "Saved";
                }
            }
            catch (Exception ex)
            {
                Status = "Fail";
            }
            return Status;
        }

        public HUDPamReportFiltersModel GetSavedFilter()
        {


            var context = this.Context as HCP_live;
            if (context == null)
                throw new InvalidCastException("context is not from db live in AddFHARequest, please pass in correct context in unit of work.");

            var CheckFilter = ((from a in context.HUD_PamReportFilters where a.CreatedBy == UserPrincipal.Current.UserId && a.UserRole == UserPrincipal.Current.UserRole select a).FirstOrDefault());

            var Vm = Mapper.Map<HUD_PamReportFilters, HUDPamReportFiltersModel>(CheckFilter);


            return Vm;
        }
    }
}
