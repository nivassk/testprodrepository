﻿using HUDHealthcarePortal.Repository;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityObject.Entities.HCP_task;
using HUDHealthcarePortal.Core;
using AutoMapper;
using Model.Production;
using Repository.Interfaces.Production;

namespace Repository
{

    public class DocumentTypeRepository : BaseRepository<DocumentTypes>, IDocumentTypeRespository
    {
        public DocumentTypeRepository()
            : base(new UnitOfWork(DBSource.Task))
        {
        }
        public DocumentTypeRepository(UnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        public IQueryable<DocumentTypes> DataToJoin
        {
            get { return DbQueryRoot; }
        }
        
        public DocumentTypeModel GetDocumentTypeById(int id)
        {
            var documenttype = this.GetByIDFast(id);
            return Mapper.Map<DocumentTypeModel>(documenttype);
        }
        public List<DocumentTypeModel> GetAllDocTypes()
        {
            var documentType = this.GetAll();
            return Mapper.Map<List<DocumentTypeModel>>(documentType);
        }
        
        public DocumentTypeModel GetDocumentTypeIDByShortname(string ShortName, int folderksy)
        {
            var context = (HCP_task)this.Context;
            
           

            var docTypeID = this.Find(x => x.ShortDocTypeName == ShortName && x.FolderKey == folderksy).FirstOrDefault();
            if (docTypeID==null)
            {
                var ParentFolderKey = (from n in context.Prod_SubFolderStructure where n.ParentKey == folderksy select n).FirstOrDefault();
                 docTypeID = this.Find(x => x.ShortDocTypeName == ShortName && x.FolderKey == ParentFolderKey.ParentKey).FirstOrDefault();
            }
            return Mapper.Map<DocumentTypeModel>(docTypeID);
        }
        public DocumentTypeModel GetDocumentTypeId(string filename, int folderkey)
        {
            var DocType = new DocumentTypeModel();
            string DocTypeShortName = GetUntilOrEmpty(filename, "_");
            int DocTypeID = -1;
            if (!string.IsNullOrEmpty(DocTypeShortName))
            {
                try
                {
                    var obj = GetDocumentTypeIDByShortname(DocTypeShortName, folderkey);
                    if (obj != null)
                    {
                        DocTypeID = obj.DocumentTypeId;

                        if (DocTypeID >= 0)
                        {
                            DocType.DocumentTypeId = DocTypeID;
                            DocType.ShortDocTypeName = DocTypeShortName;
                            return DocType;
                        }

                    }


                }
                catch (Exception ex)
                {

                }
            }
            
            return DocType;
        }
        private string GetUntilOrEmpty(string text, string stopAt)
        {
            if (!String.IsNullOrWhiteSpace(text))
            {
                if (text.ToLower().Contains(stopAt))
                {
                    int charLocation = text.IndexOf(stopAt, StringComparison.Ordinal);

                    if (charLocation > 0)
                    {
                        return text.Substring(0, charLocation);
                    }
                }

            }

            return String.Empty;
        }
    }
}
