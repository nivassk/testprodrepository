﻿using Model.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Interfaces.Production
{
  public interface IProd_NoteManager
    {
        int AddNote(Prod_NoteModel model);
        List<Prod_NoteModel> GetAllTaskNotes(Guid taskInstanceId);
        void DeleteNote(int noteId);
    }
}
