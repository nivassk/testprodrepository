﻿using System;
using BusinessService.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Model;
using Repository.Interfaces;
using Repository.ManagementReport;

namespace BusinessService.ManagementReport
{
    public class ProjectReportsManager : IProjectReportsManager
    {
        private IProjectReportsRepository _projectReportsRepository;
        private UnitOfWork unitOfWorkLive;

        public ProjectReportsManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            _projectReportsRepository = new ProjectReportsRepository(unitOfWorkLive);
        }

        public ProjectReportModel GetSuperUserProjectSummary(string userName, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportSummary(ReportLevel.SuperUserLevel, HUDRole.SuperUser, userName, "", "", "", ReportType.ProjectReport, minUploadDate, maxUploadDate, false, false, false, false, false, false);
        }

        public ProjectReportModel GetWLMProjectSummary(string userName, string wlmId, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportSummary(ReportLevel.WLMUserLevel, HUDRole.WorkflowManager, userName, wlmId, "", "", ReportType.ProjectReport, minUploadDate, maxUploadDate, false, false, false, false, false, false);
        }

        public ProjectReportModel GetAEProjectSummary(string userName, string wlmId, string aeId, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportSummary(ReportLevel.AEUserLevel, HUDRole.AccountExecutive, userName, wlmId, aeId, "", ReportType.ProjectReport, minUploadDate, maxUploadDate, false, false, false, false, false, false);
        }

        public ProjectReportModel GetProjectLenderDetail(string userName, string wlmId, string aeId, string lenderId, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportDetail(wlmId, aeId, lenderId, ReportType.ProjectReport, minUploadDate, maxUploadDate, false, false, false, false, false, false);
        }

        public ProjectReportModel GetSuperUserLargeLoanSummary(string userName, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportSummary(ReportLevel.SuperUserLevel, HUDRole.SuperUser, userName, "", "", "", ReportType.LargeLoanReport, minUploadDate, maxUploadDate, true, false, false, false, false, false);
        }

        public ProjectReportModel GetWLMLargeLoanSummary(string userName, string wlmId, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportSummary(ReportLevel.WLMUserLevel, HUDRole.WorkflowManager, userName, wlmId, "", "", ReportType.LargeLoanReport, minUploadDate, maxUploadDate, true, false, false, false, false, false);
        }

        public ProjectReportModel GetAELargeLoanSummary(string userName, string wlmId, string aeId, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportSummary(ReportLevel.AEUserLevel, HUDRole.WorkflowManager, userName, wlmId, aeId, "", ReportType.LargeLoanReport, minUploadDate, maxUploadDate, true, false, false, false, false, false);
        }

        public ProjectReportModel GetLargeLoanLenderDetail(string userName, string wlmId, string aeId, string lenderId, DateTime minUploadDate, DateTime maxUploadDate)
        {
            return GetProjectReportDetail(wlmId, aeId, lenderId, ReportType.LargeLoanReport, minUploadDate, maxUploadDate, true, false, false, false, false, false);
        }

        public ProjectReportModel GetSuperUserRatioExceptionSummary(string userName, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            return GetProjectReportSummary(ReportLevel.SuperUserLevel, HUDRole.SuperUser, userName, "", "", "", ReportType.RatioExceptionsReport, minUploadDate, maxUploadDate, false, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
        }

        public ProjectReportModel GetWLMRatioExceptionSummary(string userName, string wlmId, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            return GetProjectReportSummary(ReportLevel.WLMUserLevel, HUDRole.WorkflowManager, userName, wlmId, "", "", ReportType.RatioExceptionsReport, minUploadDate, maxUploadDate, false, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
        }

        public ProjectReportModel GetAERatioExceptionSummary(string userName, string wlmId, string aeId, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            return GetProjectReportSummary(ReportLevel.AEUserLevel, HUDRole.AccountExecutive, userName, wlmId, aeId, "", ReportType.RatioExceptionsReport, minUploadDate, maxUploadDate, false, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
        }

        public ProjectReportModel GetRatioExceptionLenderDetail(string userName, string wlmId, string aeId, string lenderId, DateTime minUploadDate, DateTime maxUploadDate, bool isDebtCoverageRatio, bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            return GetProjectReportDetail(wlmId, aeId, lenderId, ReportType.RatioExceptionsReport, minUploadDate, maxUploadDate, false, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
        }

        public ProjectReportModel GetProjectReportSummary(ReportLevel reportLevel, HUDRole hudRole, string userName, string wlmId, string aeId,
            string lenderId, ReportType reportType, DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var results = _projectReportsRepository.GetProjectReportSummary(reportLevel, hudRole, userName, wlmId, aeId, lenderId, reportType, minUploadDate, maxUploadDate, isHighLoan, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);

            return results;
        }

        public ProjectReportModel GetProjectReportDetail(string wlmId, string aeId, string lenderId, ReportType reportType,
            DateTime minUploadDate, DateTime maxUploadDate, bool isHighLoan, bool isDebtCoverageRatio,
            bool isWorkingCapital, bool isDaysCashOnHand, bool isDaysInAcctReceivable, bool isAvgPaymentPeriod)
        {
            var results = _projectReportsRepository.GetProjectReportDetail(wlmId, aeId, lenderId, reportType, minUploadDate, maxUploadDate, isHighLoan, isDebtCoverageRatio, isWorkingCapital, isDaysCashOnHand, isDaysInAcctReceivable, isAvgPaymentPeriod);
        
            return results;
        }

        public string GetWlmId(string userName)
        {
            return _projectReportsRepository.GetWlmId(userName);
        }

        public string GetAeId(string userName)
        {
            return _projectReportsRepository.GetAeId(userName);
        }

    }
}
