﻿using BusinessService.Interfaces;
using BusinessService.Interfaces.UserLogin;
using EntityObject.Entities.HCP_live;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Model;
using Repository;
using Repository.UserLogin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService
{
  public   class ReviewerTitlesManager : IReviewerTitlesManager
    {


        private UnitOfWork unitOfWorkLive;

        private ReviewerTitlesRepository reviewerTitlesRepository;

        public ReviewerTitlesManager()
        {
            unitOfWorkLive = new UnitOfWork(DBSource.Live);
            reviewerTitlesRepository = new ReviewerTitlesRepository(unitOfWorkLive);
        }

        public List<ReviewerTitlesModel> GetReviewerTitles(int TitleTypeId)
        {
            return reviewerTitlesRepository.GetReviewerTitles(TitleTypeId);
        }



        public ReviewerTitlesModel GetReviewerTitle(int titleId)
        {
            return reviewerTitlesRepository.GetReviewerTitle(titleId);
        }
        
    }
}
