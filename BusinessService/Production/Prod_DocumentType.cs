﻿using BusinessService.Interfaces.Production;
using Model.Production;
using Repository;
using Repository.Interfaces.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class Prod_DocumentType : IProd_DocumentType
    {
        
        private IDocumentTypeRespository documentTypeRepostory;
        public Prod_DocumentType()
            : this(
                new DocumentTypeRepository())
        {

        }
        private Prod_DocumentType(IDocumentTypeRespository _documentTypeRepostory)
        {
            documentTypeRepostory = _documentTypeRepostory;
        }
       public  DocumentTypeModel GetDocumentTypeId(string filename, int folserkey)
         {
             return documentTypeRepostory.GetDocumentTypeId(filename, folserkey);
         }

        public DocumentTypeModel GetDocumentTypeById(int id)
        {
            return documentTypeRepostory.GetDocumentTypeById(id);
        }
    }
}
