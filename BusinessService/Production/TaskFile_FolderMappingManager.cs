﻿using BusinessService.Interfaces.Production;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.Repository;
using Model.Production;
using Repository.Interfaces.Production;
using Repository.Production;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessService.Production
{
    public class TaskFile_FolderMappingManager : ITaskFile_FolderMappingManager
   {
      private UnitOfWork unitOfWorkTask;
        private ITaskFile_FolderMappingRepository taskFile_FolderMappingRepository;



        public TaskFile_FolderMappingManager()
        {
            unitOfWorkTask = new UnitOfWork(DBSource.Task);

            taskFile_FolderMappingRepository = new TaskFile_FolderMappingRepository(unitOfWorkTask);
     
            
        }

       public int AddTaskFile_FolderMapping(TaskFile_FolderMappingModel model)
       {
           return taskFile_FolderMappingRepository.AddTaskFile_FolderMapping(model);
       }


       public IEnumerable<TaskFileModel> GetMappedFilesbyFolder(int[] folderkeys, Guid InstanceId)
       {
           return taskFile_FolderMappingRepository.GetMappedFilesbyFolder(folderkeys, InstanceId);
       }
   }
}
