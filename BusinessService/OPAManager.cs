﻿using BusinessService.Interfaces;
using Repository.Interfaces.OPAForm;
using Repository.OPAForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Repository.Interfaces;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using HUDHealthcarePortal.Model;

namespace HUDHealthcarePortal.BusinessService
{
    public class OPAManager : IOPAManager
        
    {
        
        private UnitOfWork unitOfWorkLive;
        private UnitOfWork unitOfWorkTask;
        private IOPARepository oPARepository;

        public OPAManager()
        {
            
            unitOfWorkLive = new UnitOfWork(DBSource.Live);      
            unitOfWorkTask = new UnitOfWork(DBSource.Task);
            oPARepository = new OPARepository(unitOfWorkTask);
        }

        public List<Model.OPAWorkProductModel> getAllOPAWorkProducts(Guid taskInstanceId, string fileType)
        {
            var opaList = oPARepository.getAllOPAWorkProducts(taskInstanceId, fileType);
            if (opaList!= null && opaList.Count() > 0)
            {
                return opaList.OrderByDescending(p => p.UploadTime).ToList();//<OPAWorkProductModel>();
            }
            return null;
            //throw new NotImplementedException();
        }
    }
}
