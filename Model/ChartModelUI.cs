﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
    public class ChartModelUI
    {
        public IList<SelectListItem> AllQuarters { get; set; }
        public String SelectedQuarter { get; set; }
        public String ReportingPeriod { get; set; }
    }
}
