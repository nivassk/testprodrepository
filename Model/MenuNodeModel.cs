﻿using HUDHealthcarePortal.Core;

namespace HUDHealthcarePortal.Model
{
    public class MenuNodeModel
    {
        public string Title { get; private set; }
        public string TitleDisplay { get; set; }
        public string Controller { get; private set; }
        public string Action { get; private set; }
        public string ParentNode { get; private set; }
        public string Roles { get; private set; }
        public int MenuLevel { get; private set; }

        public MenuNodeModel(string title, string controller, string action, string parentNode, string roles, int level)
        {
            Title = title;
            Controller = controller;
            Action = action;
            ParentNode = parentNode;
            Roles = roles;
            MenuLevel = level;
        }
    }
}
