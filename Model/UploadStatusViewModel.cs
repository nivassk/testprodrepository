﻿using System;

namespace HUDHealthcarePortal.Model
{
    public class UploadAgreementModel
    {
        public string AgreeOrNot { get; set; }
    }
    public class UploadStatusViewModel
    {
        public int LenderID { get; set; }
        public string LenderName { get; set; }
        public Nullable<int> TotalExpected { get; set; }
        public Nullable<int> Received { get; set; }
        public Nullable<int> Missing { get; set; }
        public string ReportingPeriod { get; set; }
    }
}
