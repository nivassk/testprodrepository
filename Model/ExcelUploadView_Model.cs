﻿using HUDHealthcarePortal.Core;
using System;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HUDHealthcarePortal.Model
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class ExcelUploadView_Model : FundamentalFinancial
    {
        // this view model is shared by both intermediate and live data upload table
        // use data soure or context to differientate source
        public DBSource DataSource { get; set; }

        public bool ispartial {get; set;}

        public int LDI_ID { get; set; }

        public Nullable<int> PropertyID { get; set; }

        [Display(Name = "Servicer ID")]
        public Nullable<int> LenderID { get; set; }

        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }

        [Display(Name = "Servicer Name")]
        public string ServiceName { get; set; }

        [Display(Name = "Period Ending")]
        public string PeriodEnding { get; set; }

        [Display(Name = "Months in Period")]
        public string MonthsInPeriod { get; set; }

         [Display(Name = "Months in Period")]
        public Nullable<int> MonthsInPeriod_Disp { get; set; }

        [Display(Name = "Period Ending")]
        public DateTime PeriodEnding_Disp { get; set; }

    




        [Display(Name = "Units In Facility")]
        public Nullable<int> UnitsInFacility { get; set; }

        [Display(Name = "FHA Number")]
        [RegularExpression("^[0-9]{3}-[0-9]{5}$", ErrorMessage = "FHA Number in format of ###-#####, # is number 0-9.")]
        public string FHANumber { get; set; }

        // workaround for string type for datetime and decimal (can use these correct types for better formatting)
        public DateTime? PeriodEndingDt
        {
            get
            {
                if (string.IsNullOrEmpty(PeriodEnding))
                    return null;
                else
                    return DateTime.Parse(PeriodEnding);
            }
        }

        [Display(Name = "Total Revenues")]
        public Nullable<decimal> TotalRevenues { get; set; }

        [Display(Name = "Total Operating Expenses")]
        public Nullable<decimal> TotalExpenses { get; set; }

        [Display(Name = "Actual Number Of Resident Days")]
        public Nullable<decimal> ActualNumberOfResidentDays { get; set; }

        public decimal? DebtCoverageRatio2 { get; set; }
        public decimal? NOIRatio { get; set; }
        public decimal? AverageDailyRateRatio { get; set; }
        public int? FHAQuarter { get; set; }

        [Display(Name = "FHA Insured Principal & Interest Payment")]
        public Nullable<decimal> FHAInsuredPrincipalInterestPayment { get; set; }
        
        
        
        [Display(Name = "MIP")]
        public Nullable<decimal> MortgageInsurancePremium { get; set; }

        public string Blank { get { return string.Empty; } }

        // derived values
        public bool? HasCalculated { get; set; }
        public decimal? ScoreTotal { get; set; }
        //public decimal? ScoreSum { get; set; }

        public int? ModifiedBy { get; set; }
        public int? OnBehalfOfBy { get; set; }
        public int UserID { get; set; }
        public DateTime? DataInserted { get; set; } // follow the typo from db, for automapper
        public DateTime? ModifiedOn { get; set; }

        // external model
        public List<CommentModel> Comments { get; set; }

        [Display(Name="Reporting Period")]
        public string ReportingPeriod { get; set; }
    }

    public class ReportExcelUpload_Model
    {
        public DateTime DataInserted { get; set; } // follow the typo from db, for automapper
        public string Lender_Name { get; set; }
        public int? LenderID { get; set; }
        public string UserName { get; set; }
        public int UserId { get; set; }
        public int? Total_FHANumber { get; set; }
        public int? Lender_FHANumer_Matching { get; set; }
    }
}