﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class LoanTypeModel
    {
        public int LoanTypeId { get; set; }
        public string LoanTypeName { get; set; }
    }
}
