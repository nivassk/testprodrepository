﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Production
{
    public class RestfulWebApiAddPropertyModel
    {
        public string State { get; set; }
        public string zip { get; set; }
        public string propertyId { get; set; }
        public string propertyName { get; set; }
        public string City { get; set; }
        public string loanType { get; set; }
        public string loanAmount { get; set; }
        public string fhaNumber { get; set; }
        public string lenderName { get; set; }
        public string closingDate { get; set; }

    }

}



