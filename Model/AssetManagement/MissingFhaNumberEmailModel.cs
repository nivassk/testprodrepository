﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HUDHealthcarePortal.Model;

namespace Model.AssetManagement
{
    public class MissingFhaNumberEmailModel
    {
        [Required]
        public string FhaNumber { get; set; }
        public string UserName { get; set; }
        public string PropertyName { get; set; }
        public string EmailAddress { get; set; }
        public AddressModel Address { get; set; }
    }
}
