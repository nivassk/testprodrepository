﻿namespace HUDHealthcarePortal.Model
{
    public class MultiLoanDetailModel
    {
        public string FHANumber { get; set; }
        public string Lender_Name { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string StateCode { get; set; }
        public string ZIP { get; set; }
    }
}
