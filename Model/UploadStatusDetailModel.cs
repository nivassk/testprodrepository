﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HUDHealthcarePortal.Model
{
    public class UploadStatusDetailModel
    {
        public int LenderID { get; set; }
        public string Lender_Name { get; set; }
        public string ProjectName { get; set; }
        public string FHANumber { get; set; }
        public DateTime? DateInserted { get; set; }
        public string UserName { get; set; }
        public bool? Received { get; set; }
        public string ReceivedYN
        {
            get
            {
                return Received == true ? "Yes" : "No";
            }
        }
    }
}
