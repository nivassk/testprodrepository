﻿using Core.Interfaces;
using HUDHealthcarePortal.Core;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace HUDHealthcarePortal.Model
{
    [Serializable]
    [XmlType(TypeName = "NoteKeyValuePair")]
    public class NoteKeyValuePair<K, V>
    {
        public K Key
        { get; set; }

        public V Value
        { get; set; }

        public NoteKeyValuePair()
        {

        }

        public NoteKeyValuePair(K key, V value) : this()
        {
            Key = key;
            Value = value;
        }
    }

    [Serializable]
    public class TaskUserOpenStatusModel
    {
        [XmlAttribute]
        public string Username { get; set; }
        [XmlAttribute]
        public DateTime OpenUtcTime { get; set; }

        public TaskUserOpenStatusModel()
        {

        }
        public TaskUserOpenStatusModel(string userName, DateTime openUtcDate)
        {
            Username = userName;
            OpenUtcTime = openUtcDate;
        }
    }

    [Serializable]
    public class TaskOpenStatusModel
    {
        protected List<NoteKeyValuePair<string, TaskUserOpenStatusModel>> _TaskOpenStatus { get; set; }

        public List<NoteKeyValuePair<string, TaskUserOpenStatusModel>> TaskOpenStatus
        {
            get { return _TaskOpenStatus; }
            set { _TaskOpenStatus = value; }
        }

        public TaskOpenStatusModel()
        {
            _TaskOpenStatus = new List<NoteKeyValuePair<string, TaskUserOpenStatusModel>>();
        }

        public bool HasTaskOpenByUser(string userName)
        {
            return _TaskOpenStatus.Exists(p => p.Key.Equals(userName));
        }

        public void AddTaskOpenByUser(string userName)
        {
            if (!_TaskOpenStatus.Exists(p => p.Key.Equals(userName)))
                _TaskOpenStatus.Add(new NoteKeyValuePair<string, TaskUserOpenStatusModel>(userName, new TaskUserOpenStatusModel(userName, DateTime.UtcNow)));
        }
    }
}
