﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HUDHealthcarePortal.Model
{
    public class AdhocReportViewModel
    {
        public AdhocReportViewModel()
        {
            AvailableWlms = new List<SelectListItem>();

            CheckBoxModels = new List<ColumnCheckBoxModel>();
        }

        public static int ISACTIVEALL = 0;
        public static int ISACTIVE = 1;
        public static int ISCLOSED = 2;
        public List<ColumnCheckBoxModel> CheckBoxModels { get; set; }
        public string PartialFhaNumber { get; set; }
        public List<SelectListItem> AvailableWlms { get; set; }
        public string SelectedWlms { get; set; }
        public List<SelectListItem> AvailableAes { get; set; }
        public string SelectedAes { get; set; }
        public IEnumerable<SelectListItem> SelectedAesen { get; set; }
        public List<SelectListItem> AvailableLenders { get; set; }
        public string SelectedLenders { get; set; }
        public int ActiveAllSelected { get; set; }

        [Display(Name = "Min Period Ending")]
        public string MinPeriodEnding { get; set; }
        // workaround for string type for datetime and decimal (can use these correct types for better formatting)
        public DateTime? MinPeriodEndingDt
        {
            get
            {
                if (string.IsNullOrEmpty(MinPeriodEnding))
                    return null;
                else
                    return DateTime.Parse(MinPeriodEnding);
            }
        }

        [Display(Name = "Max Period Ending")]
        public string MaxPeriodEnding { get; set; }
        // workaround for string type for datetime and decimal (can use these correct types for better formatting)
        public DateTime? MaxPeriodEndingDt
        {
            get
            {
                if (string.IsNullOrEmpty(MaxPeriodEnding))
                    return null;
                else
                    return DateTime.Parse(MaxPeriodEnding);
            }
        }
    }

    public class ColumnCheckBoxModel
    {
        public string ColumnName { get; set; }
        public int Id { get; set; }
        public bool Selected { get; set; }
    }
}
