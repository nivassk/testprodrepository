﻿-- These insert statements should be executed on the SQL DB

-----------------------------------------------
------ Insert into NonCriticalLateTimeSpan

insert into NonCriticalLateTimeSpan values (0, '90 Days');
insert into NonCriticalLateTimeSpan values (1, '180 Days');
insert into NonCriticalLateTimeSpan values (2, '270 Days');
insert into NonCriticalLateTimeSpan values (3, '270 Days Over 50%');

-----------------------------------------------
------ Insert new email type in HCP_EmailType

insert into HCP_EmailType (EmailTypeId, EmailTypeCd, EmailTypeDescription, Deleted_ind)
values (13, 'NCRENA1', 'Tier 1 Email Notification to submit NCR', 0);

insert into HCP_EmailType (EmailTypeId, EmailTypeCd, EmailTypeDescription, Deleted_ind)
values (14, 'NCRENA2', 'Tier 2 Email Notification to submit NCR', 0);

-----------------------------------------------
------ Insert new email type in HCP_EmailTemplateMessages

insert into HCP_EmailTemplateMessages (MessageTemplateId, EmailTypeId, MessageSubject, MessageBody, KeyWords, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
values (1, 13, 'Request for Non-Critical Repair Request Form Submission', 
'This notification is for [LENDER].  FHA Number [FHANUMBER] for property [PROPERTY] is [DAYS] days past due. Submit a Non-Critical Repair Request form on the HUD Web Portal.', 
'[LENDER],[FHANUMBER],[PROPERTY],[DAYS]', GETDATE(), 0, GETDATE(), 0);

insert into HCP_EmailTemplateMessages (MessageTemplateId, EmailTypeId, MessageSubject, MessageBody, KeyWords, CreatedOn, CreatedBy, ModifiedOn, ModifiedBy)
values (2, 14, 'Request for Non-Critical Repair Request Form Submission', 
'This notification is for [LENDER].  FHA Number [FHANUMBER] for property [PROPERTY] has remaining balance greater than [PERCENT]%. Submit a Non-Critical Repair Request form on the HUD Web Portal.', 
'[LENDER],[FHANUMBER],[PROPERTY],[PERCENT]', GETDATE(), 0, GETDATE(), 0);
