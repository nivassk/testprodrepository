﻿using System;
using System.Collections.Generic;
using BusinessService.Interfaces;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using HUDHealthcarePortal.Repository;
using Repository.Interfaces;

namespace HUDEmailNotificationApp.Classes
{
    public class EmailNotification : IEmailNotification
    {
        private IEmailManager emailManager;
        private INonCriticalLateNotification ncrLateNotification;
        private IAccountManager accountManager;
        
        public EmailNotification()
        {
            emailManager = new EmailManager();
            accountManager = new AccountManager();
        }

        public int SendEmails(EmailType emailType, string keywords, string userIds)
        {
            return emailManager.SendEmail(emailType, keywords, userIds, true);
        }
    }
}