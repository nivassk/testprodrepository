CREATE PROCEDURE [dbo].[usp_HCP_CalculateErrors]
@QueryDate NVARCHAR( 50 )
AS

--UPDATE dbo.Lender_DataUpload_Intermediate SET HasError=1,TotalErrors=1
BEGIN try
        Begin transaction


declare @RowNum INT ,@FHANumber varchar (30 ), @PEYear INT , @LDI_ID int , @MIP decimal ,@PE DATETIME , @IMDate DATETIME, @LDI_ID_Top1 INT, @ErrorRecorded int

declare @dt  Datetime


set @dt= convert( datetime , @QueryDate , 101 )


--select @RowNum = min( RowNum ) from @DistinctFHAYEAR
declare @LDI_ID_Table table (
LDI_ID int
) 
insert into @LDI_ID_Table select LDI_ID  from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a where     CONVERT (VARCHAR ( 10), cast (a . datainserted as date), 101 )>@dt  and    a. IsErrorProcessed is null

select @LDI_ID = min( LDI_ID ) from @LDI_ID_Table


while @LDI_ID is not null
begin

declare @TempData_Error TABLE (
LDI_ID  int,
FHANumber nvarchar( 50 )  NULL,
MonthsInPeriod DECIMAL( 19 ,2 )   NULL,
QuaterlyNoi DECIMAL( 19 ,2 ) NULL,
DataInserted nvarchar( 50 ) NULL,
NOIQuaterbyQuater DECIMAL( 19 ,2 ) NULL,
NOIPercentageChange DECIMAL( 19 ,2 ) NULL,
NOICumulative DECIMAL( 19 ,2 ) NULL,
CumulativeRevenue DECIMAL( 19 ,2 ) NULL,
CumulativeResidentDays DECIMAL (19 , 2) NULL,
AverageDailyRateRatio DECIMAL( 19 ,2 ) NULL,
CumulativeExpenses DECIMAL( 19 ,2 ) NULL,
FHAPICumulative DECIMAL( 19 ,2 ) NULL,
MIPCumulative  DECIMAL( 19 ,2 ) NULL,
QuarterlyDSCR DECIMAL( 19 ,2 ) NULL,
TotalErrors INT NULL,
QuaterlyOperatingRevenue  DECIMAL (19 , 2) NULL,
QuarterlyOperatingExpense DECIMAL (19 , 2) NULL,
ADRDifferencePerQuarter DECIMAL (19 , 2) NULL

)

select    @MIP= MonthsInPeriod ,@PEYear = YEAR( CONVERT( DATETIME , PeriodEnding)),@PE= CONVERT( DATETIME , PeriodEnding),@FHANumber = FHANumber from [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID =@LDI_ID
--select    @PEYear = YEAR( CONVERT( DATETIME , PeriodEnding)) from [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID
--select    @PE=     CONVERT( DATETIME , PeriodEnding) from [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID
--select    @FHANumber = FHANumber from  [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] where LDI_ID = @LDI_ID

IF (@MIP = 3)
BEGIN
SET @IMDate= @pe
END
ELSE IF ( @MIP = 6 )
BEGIN
SELECT @IMDate= DATEADD ( MONTH ,- 3, @pe )
END
ELSE IF ( @MIP = 9 )
BEGIN
SELECT @IMDate= DATEADD ( MONTH ,- 6, @pe )
END
ELSE IF ( @MIP = 12 )
BEGIN
SELECT @IMDate= DATEADD ( MONTH ,- 9, @pe )
End


delete from @TempData_Error
insert into @TempData_Error  SELECT DISTINCT a. LDI_ID , FHANumber , MonthsInPeriod, QuaterlyNOI, DataInserted, NOIQuaterbyQuater ,NOIPercentageChange , NOICumulative, CumulativeRevenue , CumulativeResidentDays , AverageDailyRateRatio, CumulativeExpenses , FHAPICumulative , MIPCumulative, QuarterlyDSCR , TotalErrors , QuaterlyOperatingRevenue, QuarterlyOperatingExpense , ADRDifferencePerQuarter
from [$(DatabaseName)].dbo.[fn_HCP_GeLatestUpload] () a    where      a. FHANumber= @FHANumber and CONVERT ( DATETIME , a. PeriodEnding )BETWEEN   @IMDate AND @PE AND a .MonthsInPeriod <= @MIP AND a .HasCalculated = 1 and CONVERT( VARCHAR ( 10), cast( a .datainserted as date ), 101 )> @dt

--SELECT * FROM @TempData_Error
DECLARE @NoILessthanZero INT ,@DSCRLessthanZero INT

--Logic for Checking if any QuarterlyNoi Less than 0--
SELECT @NoILessthanZero= COUNT(*) FROM @TempData_Error WHERE QuaterlyNoi<= 0.0
IF (@NoILessthanZero > 0)
BEGIN
--UPDATE [dbo]. [Lender_DataUpload_Intermediate] SET TotalErrors= 1,HasError=1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM  @TempData_Error WHERE QuaterlyNoi<0.0) AND HasError IS NULL

SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2  WHERE LDI_ID IN ( SELECT  LDI_ID FROM   @TempData_Error WHERE    LDI_ID<> @LDI_ID_Top1)
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE QuaterlyNoi< 0.0 )
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT     DISTINCT LDI_ID  FROM   @TempData_Error WHERE QuaterlyNoi< 0.0 )
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 , 12.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyNOIPTError= 1, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT     DISTINCT LDI_ID  FROM   @TempData_Error WHERE QuaterlyNoi <0.0 )
END

--Logic for Checking if any DSCR Less than 1.2--
SELECT @DSCRLessthanZero= COUNT(*) FROM @TempData_Error WHERE QuarterlyDSCR< 1.2
IF (@DSCRLessthanZero > 0)
BEGIN

SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2  WHERE LDI_ID IN ( SELECT  LDI_ID FROM   @TempData_Error WHERE    LDI_ID<> @LDI_ID_Top1)
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE QuarterlyDSCR< 1.2 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  QuarterlyDSCR< 1.2 )
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 , 12.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyDSCRPTError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE QuarterlyDSCR <1.2 )
END

IF (@MIP = 9 or @MIP = 12 )
BEGIN
-------------Calculation of  Quarterly NOI Dip-----------------
DECLARE @NOIQ1 DECIMAL ( 19, 2 ) , @NOIQ2 DECIMAL ( 19, 2 ),@NOIQ3 DECIMAL (19 , 2), @NOIQ4 DECIMAL (19 , 2)

SELECT @NOIQ1 = ISNULL( QuaterlyNoi ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 3.00
SELECT @NOIQ2 = ISNULL( QuaterlyNoi ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 6.00
SELECT @NOIQ3 = ISNULL( QuaterlyNoi ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 9.00
SELECT @NOIQ4 = ISNULL( QuaterlyNoi ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 12.00

IF (@MIP = 9 AND( @NOIQ2 <@NOIQ1 ) AND (@NOIQ3 < @NOIQ2))
BEGIN

SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 6.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 6.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyNOIError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN(6.00 , 9.00))

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 ))
--UPDATE [dbo]. [Lender_DataUpload_Intermediate] SET TotalErrors= 1  WHERE LDI_ID=(SELECT TOP LDI_ID FROM @TempData_Error WHERE MonthsInPeriod=6.00) AND HasError IS NULL
--UPDATE [dbo]. [Lender_DataUpload_Intermediate] SET HasError=1  WHERE LDI_ID IN  (SELECT DISTINCT LDI_ID FROM  @TempData_Error WHERE MonthsInPeriod IN(6.00,9.00))

END
ELSE IF ( @MIP = 12 AND ( @NOIQ3< @NOIQ2 ) AND ( @NOIQ4 <@NOIQ3 ))
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 9.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 12.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyNOIError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN(12.00 , 9.00))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 , 12.00 ))

END

-------------Calculation of  Quarterly Operating Revenue Dip-----------------
DECLARE @QORQ1 DECIMAL ( 19, 2 ) , @QORQ2 DECIMAL ( 19, 2 ),@QORQ3 DECIMAL (19 , 2), @QORQ4 DECIMAL (19 , 2)

SELECT @QORQ1 = ISNULL( QuaterlyOperatingRevenue ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 3.00
SELECT @QORQ2 = ISNULL( QuaterlyOperatingRevenue ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 6.00
SELECT @QORQ3 = ISNULL( QuaterlyOperatingRevenue ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 9.00
SELECT @QORQ4 = ISNULL( QuaterlyOperatingRevenue ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 12.00

IF (@MIP = 9 AND (@QORQ2 < @QORQ1) AND ( @QORQ3< @QORQ2 ))
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2  WHERE LDI_ID IN ( SELECT  LDI_ID FROM   @TempData_Error WHERE    LDI_ID<> @LDI_ID_Top1)
END
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 6.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 6.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyORevError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN(6.00 , 9.00))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 ))
--UPDATE [dbo]. [Lender_DataUpload_Intermediate] SET TotalErrors= 1  WHERE LDI_ID=(SELECT TOP LDI_ID FROM @TempData_Error WHERE MonthsInPeriod=6.00) AND HasError IS NULL
--UPDATE [dbo]. [Lender_DataUpload_Intermediate] SET HasError=1  WHERE LDI_ID IN  (SELECT DISTINCT LDI_ID FROM  @TempData_Error WHERE MonthsInPeriod IN(6.00,9.00))

END
ELSE IF ( @MIP = 12 AND ( @QORQ3< @QORQ2 ) AND ( @QORQ4 <@QORQ3 ))
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 9.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 12.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyORevError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN(12.00 , 9.00))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 , 12.00 ))

END


-------------Calculation of  Quarterly DSCR Dip-----------------
DECLARE @DSCRQ1 DECIMAL ( 19, 2 ) , @DSCRQ2 DECIMAL ( 19, 2 ),@DSCRQ3 DECIMAL (19 , 2), @DSCRQ4 DECIMAL (19 , 2)

SELECT @DSCRQ1 = ISNULL( QuarterlyDSCR ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 3.00
SELECT @DSCRQ2 = ISNULL( QuarterlyDSCR ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 6.00
SELECT @DSCRQ3 = ISNULL( QuarterlyDSCR ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 9.00
SELECT @DSCRQ4 = ISNULL( QuarterlyDSCR ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 12.00

IF (@MIP = 9 AND (@DSCRQ2 < @DSCRQ1) AND ( @DSCRQ3< @DSCRQ2 ))
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2 , IsErrorProcessed =1 WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 6.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 6.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyDSCRError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN(6.00 , 9.00))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 ))


END
ELSE IF ( @MIP = 12 AND ( @DSCRQ3< @DSCRQ2 ) AND ( @DSCRQ4 <@DSCRQ3 ))
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 9.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 12.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsQtrlyDSCRError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN(12.00 , 9.00))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 , 12.00 ))

END



-------------Calculation of  Quarterly ADR Dip-----------------
DECLARE @ADRPercntQ1 DECIMAL ( 19, 2 ) , @ADRPercntQ2 DECIMAL ( 19, 2 ), @ADRPercntQ3 DECIMAL (19 , 2), @ADRPercntQ4 DECIMAL( 19 ,2 ), @ADRDifferenceQ3 DECIMAL ( 19 , 2 ), @ADRDifferenceQ4 DECIMAL ( 19 , 2 )


SELECT @ADRPercntQ2 = ISNULL( ADRDifferencePerQuarter ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 6.00
SELECT @ADRPercntQ3 = ISNULL( ADRDifferencePerQuarter ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 9.00
SELECT @ADRPercntQ4 = ISNULL( ADRDifferencePerQuarter ,0 ) FROM @TempData_Error WHERE MonthsInPeriod= 12.00


  IF( @MIP = 12 AND ( @ADRPercntQ3 <@ADRPercntQ2 ) AND (@ADRPercntQ4 < @ADRPercntQ3))
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1, IsErrorProcessed =1  WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 9.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 12.00 ,9.00 ))

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsADRError= 1,  IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 12.00 ,9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod IN( 3.00 , 6.00 , 9.00 , 12.00 ))

END



  ELSE  IF (@MIP = 12 AND (@ADRPercntQ3 - @ADRPercntQ2)>=- 7.5 or ( @ADRPercntQ4- @ADRPercntQ3)>=- 7.5 )
BEGIN
SELECT @ErrorRecorded= COUNT(*) FROM @TempData_Error WHERE Totalerrors= 1
IF (@ErrorRecorded > 0)
BEGIN
SELECT @LDI_ID_Top1=    LDI_ID FROM @TempData_Error WHERE Totalerrors= 1
PRINT @LDI_ID_Top1
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 2, IsErrorProcessed =1  WHERE LDI_ID IN (SELECT    LDI_ID FROM  @TempData_Error WHERE   LDI_ID<> @LDI_ID_Top1 )
END

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET TotalErrors= 1 , IsErrorProcessed =1 WHERE LDI_ID= (SELECT TOP 1 LDI_ID FROM   @TempData_Error WHERE MonthsInPeriod= 9.00 ) AND HasError IS NULL
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET HasError= 1, IsErrorProcessed =1  WHERE LDI_ID IN    (SELECT DISTINCT LDI_ID FROM   @TempData_Error WHERE  MonthsInPeriod IN( 12.00 ,9.00 ))

UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsADRError= 1  WHERE LDI_ID IN  ( SELECT DISTINCT LDI_ID FROM    @TempData_Error WHERE MonthsInPeriod IN( 12.00 , 9.00 ))
UPDATE [$(DatabaseName)].dbo.[Lender_DataUpload_Intermediate] SET IsProjectError= 1  WHERE LDI_ID IN  ( SELECT DISTINCT LDI_ID FROM    @TempData_Error WHERE MonthsInPeriod IN( 3.00 ,6.00 , 9.00, 12.00 ))

END




END



select @LDI_ID = min( LDI_ID ) from @LDI_ID_Table where LDI_ID >   @LDI_ID 
END

Update [dbo].[Lender_DataUpload_Intermediate] set IsProjectError=null where IsProjectError=1 and [IsQtrlyORevError] is null and [IsQtrlyNOIError] is null and [IsQtrlyNOIPTError] is null and [IsQtrlyDSCRError] is null and [IsQtrlyDSCRPTError] is null and [IsADRError] is null

Commit transaction
      
End try
Begin catch
        print ERROR_MESSAGE ()
        Rollback transaction
End catch

