﻿CREATE  PROCEDURE [dbo].[GetAelistforMail](
   @Taskinstanceid uniqueidentifier
   
)
AS
    BEGIN
	DECLARE @listStr VARCHAR(MAX)

	SELECT distinct @listStr = COALESCE(@listStr+';' ,'') +  SPAE.Email
 from [$(LiveDB)].[dbo].[Prod_SharepointScreen] SP 
			
				 join [$(LiveDB)].[dbo].[Prod_SharePointAccountExecutives] SPAE
				 on SPAE.sharepointaeid=sp.selectedaeid 
				 where SP.taskinstanceid=@Taskinstanceid

				 select  @listStr as AElist
  end

