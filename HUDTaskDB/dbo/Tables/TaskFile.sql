﻿CREATE TABLE [dbo].[TaskFile](
	[FileId] [int] IDENTITY(1,1) NOT NULL,
	[TaskFileId] [uniqueidentifier] ROWGUIDCOL  NOT NULL,
	[FileName] [nvarchar](max) NOT NULL,
	[FileData] [varbinary](max) FILESTREAM  NULL,
	[TaskInstanceId] [uniqueidentifier] NOT NULL,
	[UploadTime] [datetime] NOT NULL,
	[FileType] [nchar](100) NULL,
	[SystemFileName] [varchar](200) NULL,
	[CreatedBy] [int] NULL,
	[FileSize] [float] NULL,
	[API_upload_status] [varchar](20) NULL,
	[Version] [int] NULL,
	[DocId] [varchar](20) NULL,
	[DocTypeID] [varchar](20) NULL,
	[RoleName] [NVARCHAR](100) NULL
	CONSTRAINT [PK_TaskFile_FileId] PRIMARY KEY CLUSTERED 
	(
		[FileId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON),
    CONSTRAINT [UK_TaskFile_FileId] UNIQUE NONCLUSTERED 
	(
		[TaskFileId] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY] FILESTREAM_ON [HCP_Task_FileStream]

GO
--SET ANSI_PADDING OFF
--GO

ALTER TABLE [dbo].[TaskFile] ADD  DEFAULT ((0)) FOR [FileSize]
GO