﻿CREATE TABLE [dbo].[DocumentTypes](
	[DocumentTypeId] [int] NOT NULL,
	[DocumentType] [varchar](500) NOT NULL,
	[ShortDocTypeName] [varchar](20) NULL,
	[FolderKey] [int] NULL, 
    [Sorting] VARCHAR(20) NULL
) ON [PRIMARY]

