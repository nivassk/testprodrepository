﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessService.Interfaces;
using BusinessService.Interfaces.Production;
using BusinessService.Production;
using Core;
using HUDHealthcarePortal.BusinessService;
using HUDHealthcarePortal.Core;
using Model.Production;
using HUDHealthcarePortal.Model;
using HUDHealthcarePortal.BusinessService.Interfaces;
using BusinessService.ProjectAction;
using HUDHealthcarePortal.Core.Utilities;
using System.Text;
using HUDHealthcarePortal.Helpers;
using BusinessService.Interfaces.InternalExternalTask;
using BusinessService.InternalExternalTask;

namespace HUDHealthcarePortal.Controllers.Production
{
    public class ProductionMyTaskController : Controller
    {
        //
        // GET: /ProductionMyTask/
        private IProductionQueueManager _productionQueueManager;
        private IProd_TaskXrefManager _prodTaskXrefManager;
        private ITaskManager _taskManager;
        private IFHANumberRequestManager _fhaNumberRequestManager;
        private IParentChildTaskManager _parentChildTaskManager;
        private IProjectActionFormManager _projectActionFormManager;
        private IAccountManager accountManager;
        private IInternalExternalTaskManager internalExternalTaskManger;//skumar-form290
        public ProductionMyTaskController()
            : this(new ProjectActionFormManager(), new ParentChildTaskManager(), new ProductionQueueManager(), new Prod_TaskXrefManager(), new TaskManager(), new FHANumberRequestManager(), new AccountManager(), new InternalExternalTaskManager())
        {
            
        }

        public ProductionMyTaskController(IProjectActionFormManager projectActionFormManager, IParentChildTaskManager parentChildTaskManager, IProductionQueueManager productionQueueManager, IProd_TaskXrefManager prodTaskXrefManager,
                                          ITaskManager taskManager,IFHANumberRequestManager fhaNumberRequestManager, IAccountManager _accountManager, IInternalExternalTaskManager _internalExternalTaskManager)
        {
            _projectActionFormManager = projectActionFormManager;
            _parentChildTaskManager = parentChildTaskManager;
           _productionQueueManager = productionQueueManager;
            _prodTaskXrefManager = prodTaskXrefManager;
            _taskManager = taskManager;
            _fhaNumberRequestManager = fhaNumberRequestManager;
            accountManager = _accountManager;
            internalExternalTaskManger = _internalExternalTaskManager;
        }
        public ActionResult Index()
        {
            var userName = UserPrincipal.Current.UserName;
            if (RoleManager.IsUserLenderRole(userName) || RoleManager.IsInspectionContractor(userName))
            {
              
                return View("~/Views/Production/ProductionMyTask/MyTaskLenderView.cshtml");
            }
           
            return View("~/Views/Production/ProductionMyTask/Index.cshtml");
        }

        public JsonResult GetMyTasks(string sidx, string sord, int page, int rows)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            var currentUser = UserPrincipal.Current;
            int form290Request = (int)TaskStep.Form290Request;
            int form290Complete = (int)TaskStep.Form290Complete;

            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole)
                                .OrderByDescending(m=>m.ProductionTaskType[0]).ThenByDescending(m=>m.LastUpdated);
            
            
            if (productionTasks != null)
            {
                foreach (var item in productionTasks)
                {
                    item.LastUpdated = TimezoneManager.ToMyTimeFromUtc(item.LastUpdated);
					if (item.ProductionTaskType == "AMENDMENTS")
						item.ProductionTaskType = "AMENDMENTS REQUEST";
                }
            }
            int totalrecods = productionTasks.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
			productionTasks = productionTasks.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(o => o.orderby);
			var results = productionTasks.Skip(pageIndex * pageSize).Take(pageSize);
			var outresults = results.GroupBy(s => s.ProductionTaskType).SelectMany(gr => gr).ToList().GroupBy(s => s.groupid).SelectMany(gr => gr).OrderBy(o=>o.orderby).ToList();
            var olstForm290 = outresults.Where(m => m.ProductionType == "Form 290").ToList();
            if (olstForm290.Count > 0)
            {
                var duplicateKeys = olstForm290.GroupBy(x => x.TaskName)
                        .Where(group => group.Count() > 1)
                        .Select(group => group.Key);

                foreach (string item in duplicateKeys)
                {
                    var dupNames = olstForm290.Where(m => m.TaskName == item).ToList();
                    ProductionMyTaskModel objForm290Request = dupNames.Where(m => m.StatusId == form290Request).FirstOrDefault();
                    ProductionMyTaskModel objForm290Complete = dupNames.Where(m => m.StatusId == form290Complete).FirstOrDefault();

                    if (objForm290Complete != null && objForm290Request != null)
                        outresults.Remove(objForm290Request);
                }
            }

            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = outresults,

            };

            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProductionTaskDetail(Guid parentChildInstanceId, int fhaRequestType)
        {
            
            var fhaRequestViewModel = _fhaNumberRequestManager.GetFhaRequestByTaskInstanceId(parentChildInstanceId);
            if (Request.QueryString.Count > 2)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    fhaRequestViewModel.IsLenderPAMReport = true;
                }
                else
                {
                    fhaRequestViewModel.IsPAMReport = true;
                }
                
            }
            FHARequestController.SetModelValues(fhaRequestViewModel);
            fhaRequestViewModel.LenderUserId = fhaRequestViewModel.CreatedBy;
            fhaRequestViewModel.LenderUserName = accountManager.GetUserById(fhaRequestViewModel.CreatedBy).UserName;
            var fhaController = new FHARequestController();
            fhaController.PopulateWeeklyFeedFHANumberList(fhaRequestViewModel);
            FHARequestController.SetDropdownValuesForFHARequestForm(fhaRequestViewModel);
            fhaRequestViewModel.IsDisclaimerAccepted = true;
            fhaRequestViewModel.FhaRequestType = fhaRequestType;
            Prod_MessageModel checkResult = ControllerHelper.CheckTransAccess();
            fhaRequestViewModel.TransAccessStatus = checkResult.status;
            if (RoleManager.IsUserLamBamLar(UserPrincipal.Current.UserName) ||
                (fhaRequestViewModel.IsFhaInsertComplete && fhaRequestType == (int) PageType.FhaRequest)
                || (fhaRequestViewModel.IsPortfolioComplete && fhaRequestType == (int) ProductionView.Portfolio) ||
                (fhaRequestViewModel.IsCreditReviewAttachComplete && fhaRequestType == (int) ProductionView.CreditReview))
            {
                fhaRequestViewModel.IsMyTask = true;
                return View("~/Views/Production/FHARequest/FHARequestLenderReadOnlyView.cshtml", fhaRequestViewModel);
            }
            return View("~/Views/Production/FHARequest/FHARequestProductionUserView.cshtml", fhaRequestViewModel);
        }

        
        public ActionResult GetComments(Guid fhaRequestId, int type)
        {   
            var fhaNumberRequest = _fhaNumberRequestManager.GetFhaRequestById(fhaRequestId);

            if (fhaNumberRequest != null)
            {
                var subTasks = _prodTaskXrefManager.GetProductionSubTasks(fhaNumberRequest.TaskinstanceId);
                var productionUserMsg = new ProductionComments();
                LenderComment lenderComment = new LenderComment
                {
                   
                    CommentText = fhaNumberRequest.Comments,
                    LenderName = _fhaNumberRequestManager.GetLenderName(fhaNumberRequest.LenderId)
                };
                
                              
                
                if (type == (int)ProductionView.CreditReview)
                {
                    var creditReview = subTasks.FirstOrDefault(m => m.ViewId == type);
                    var prodUser = _productionQueueManager.GetUserById(creditReview.AssignedTo);
                    List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                    {
                        new ProductionUserComments()
                        {
                            CommentText = fhaNumberRequest.CreditreviewComments,
                            CommentType =
                                EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(type.ToString())),
                            ProductionUserName =string.Format("{0} {1}",prodUser.FirstName,prodUser.LastName)
                        }

                    };
                    productionUserMsg.LenderComment = lenderComment;
                    productionUserMsg.ProductionUserComments = productionUserComments;


                }
                else if (type == (int)ProductionView.Portfolio)
                {

                    var portfolio = subTasks.FirstOrDefault(m => m.ViewId == type);
                    var prodUser = _productionQueueManager.GetUserById(portfolio.AssignedTo);
                    List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                    {
                        new ProductionUserComments()
                        {
                            CommentText = fhaNumberRequest.PortfolioComments,
                            CommentType =
                                EnumType.GetEnumDescription(EnumType.Parse<ProductionView>(type.ToString())),
                            ProductionUserName = string.Format("{0} {1}",prodUser.FirstName,prodUser.LastName)
                        }

                    };
                    
                    productionUserMsg.LenderComment = lenderComment;
                    productionUserMsg.ProductionUserComments = productionUserComments;
                }
                else
                {
                    var fhaInset = _taskManager.GetTasksByTaskInstanceId(fhaNumberRequest.TaskinstanceId).LastOrDefault();
                    var creditReview = subTasks.FirstOrDefault(m => m.ViewId == (int)ProductionView.CreditReview);
                    if (creditReview != null)
                    {
                        var prodUserCR = _productionQueueManager.GetUserById(creditReview.AssignedTo);
                        var portfolio = subTasks.FirstOrDefault(m => m.ViewId == (int)ProductionView.Portfolio);
                        if (portfolio != null)
                        {   var prodUserPF = _productionQueueManager.GetUserById(portfolio.AssignedTo);
                            var prodUserFHAInsert = new UserInfoModel();
                            if (fhaInset != null)
                            {
                                prodUserFHAInsert = _productionQueueManager.GetUserInfoByUsername(fhaInset.AssignedTo);
                            }

                            List<ProductionUserComments> productionUserComments = new List<ProductionUserComments>
                            {
                                new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.InsertFHAComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.InsertFha.ToString())),
                                    ProductionUserName = prodUserFHAInsert== null?"Not Assigned":string.Format("{0} {1}",prodUserFHAInsert.FirstName,prodUserFHAInsert.LastName)
                                },
                                new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.CreditreviewComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.CreditReview.ToString())),
                                    ProductionUserName = string.Format("{0} {1}",prodUserCR.FirstName,prodUserCR.LastName)
                                },
                                new ProductionUserComments()
                                {
                                    CommentText = fhaNumberRequest.PortfolioComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.Portfolio.ToString())),
                                    ProductionUserName = string.Format("{0} {1}",prodUserPF.FirstName,prodUserPF.LastName)
                                }
                            };
                    
                            productionUserMsg.LenderComment = lenderComment;
                            productionUserMsg.ProductionUserComments = productionUserComments;
                        }
                    }
                     else
                    {
                        var prodUserFHAInsert = new UserInfoModel();
                        if (fhaInset != null)
                        {
                            prodUserFHAInsert = _productionQueueManager.GetUserInfoByUsername(fhaInset.AssignedTo);
                        }
                        productionUserMsg.LenderComment = lenderComment;
                        productionUserMsg.ProductionUserComments =new List<ProductionUserComments> {
                            new ProductionUserComments
                                {
                                    CommentText = fhaNumberRequest.InsertFHAComments,
                                    CommentType =
                                        EnumType.GetEnumDescription(
                                            EnumType.Parse<ProductionView>(ProductionView.InsertFha.ToString())),
                                    ProductionUserName = prodUserFHAInsert == null ? "Not Assigned" : string.Format("{0} {1}", prodUserFHAInsert.FirstName, prodUserFHAInsert.LastName)
                                }
                                };

                    }
                }
                return View("~/Views/Production/ProductionMyTask/comments.cshtml", productionUserMsg);
            }
            
            return View("~/Views/Production/ProductionMyTask/index.cshtml");
        }
        private void MyTaskProdNumbers()
        {
            var currentUser = UserPrincipal.Current;
            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole);

            ViewBag.TotalFhaInsert = productionTasks.Where(m=>m.FhaRequestType==(int)PageType.FhaRequest).Count();
            ViewBag.TotalCreditReviews = productionTasks.Where(m => m.FhaRequestType == (int)ProductionView.CreditReview).Count();
            ViewBag.TotalPortfolios = productionTasks.Where(m => m.FhaRequestType == (int)ProductionView.Portfolio).Count();
            //ViewBag.AppProcessInProcess = productionTasks.Where(m => m.FhaRequestType == (int)PageType.ProductionApplication).Count();

        }
        private void MyTaskLenderNumbers()
        {
            var currentUser = UserPrincipal.Current;
            var productionTasks = _productionQueueManager.GetProductionTaskByUserName(currentUser.UserName, currentUser.UserRole);
            ViewBag.FHAInQueue = productionTasks.Where(m =>m.FhaRequestType==(int)PageType.FhaRequest && m.StatusId ==(int) ProductionFhaRequestStatus.InQueue).Count();
            ViewBag.FHAInProcess = productionTasks.Where(m => m.FhaRequestType == (int)PageType.FhaRequest && m.StatusId == (int)ProductionFhaRequestStatus.InProcess).Count();
            ViewBag.AppProcessInQueue = productionTasks.Where(m => m.FhaRequestType == (int)PageType.ProductionApplication && m.StatusId == (int)ProductionAppProcessStatus.InQueue).Count();
            ViewBag.AppProcessInProcess = productionTasks.Where(m => m.FhaRequestType == (int)PageType.ProductionApplication && m.StatusId == (int)ProductionAppProcessStatus.InProcess).Count();

        }

        public ActionResult GetTaskDetail(Guid taskInstanceId)
        {
            var formUploadData = new OPAViewModel();
            var isChildTask = false;
            var task = new TaskModel();
            var xrefmodel = new Prod_TaskXrefModel();

            formUploadData.isChildTaskOpen = false;

            if (_parentChildTaskManager.IsParentTaskAvailable(taskInstanceId))
            {
                task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
               
                formUploadData = _projectActionFormManager.GetProdOPAByChildTaskId(task.TaskInstanceId);
                 formUploadData.viewId = 0;
                formUploadData.ChildTaskInstanceId = task.TaskInstanceId;
                isChildTask = true;
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName) || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    formUploadData.IsLoggedInUserLender = false;
                }
            }
            else
            {
                if (RoleManager.IsUserLenderRole(UserPrincipal.Current.UserName))
                {
                    task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }

                else if (RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
                {
                    task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);
                    formUploadData.viewId = 0;
                    formUploadData.IsLoggedInUserLender = true;
                }
                else
                {
                    task = _taskManager.GetLatestTaskByTaskXrefid(taskInstanceId);
                    if(task == null)
                    {
                        task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
                        taskInstanceId = _prodTaskXrefManager.GetUnderwriterTaskXrefIdByParentTaskInstanceId(task.TaskInstanceId);
                    }
                    formUploadData = _projectActionFormManager.GetOPAByTaskInstanceId(task.TaskInstanceId);

                    //logic to check is an child open task  and for confirmation popup flag
                    formUploadData.isChildTaskOpen = false;
                    List<TaskModel> childTaskList = _projectActionFormManager.GetProdChildTasksByXrefId(task.TaskInstanceId, false);
                    if (childTaskList != null && childTaskList.Count > 0)
                    {

                        foreach (var childList in childTaskList)
                        {

                            if (childList.TaskStepId == (int)TaskStep.OPAAddtionalInformation)
                            {
                                formUploadData.isChildTaskOpen = true;
                                break;
                            }

                        }
                    }
                     xrefmodel = _taskManager.GetReviewerViewIdByXrefTaskInstanceId(taskInstanceId);
                    formUploadData.viewId = xrefmodel!=null?xrefmodel.ViewId:0;
                    formUploadData.reviertaskStatus = xrefmodel != null ?xrefmodel.Status:0;
                    formUploadData.taskxrefId = taskInstanceId;
                    formUploadData.IsLoggedInUserLender = false;
                }

            }
            // based on task step, decide if is edit mode or readonly mode
            bool isEditMode = true;
            //if (task.TaskStepId == 17)
            //{
            //    task.TaskStepId = 14;
            //}

            if(xrefmodel!= null && xrefmodel.CompletedOn != null && xrefmodel.Status == (int) TaskStep.ProjectActionRequestComplete )
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequestComplete)
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.ProjectActionRequest && RoleManager.IsCurrentUserLenderRoles() ||RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)TaskStep.OPAAddtionalInformation && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            else if (task.TaskStepId == (int)ProductionAppProcessStatus.Request && !RoleManager.IsCurrentUserLenderRoles() || RoleManager.IsInspectionContractor(UserPrincipal.Current.UserName))
            {
                isEditMode = false;
            }
            


            bool hasTaskOpenByUser = false;
            formUploadData.IschildTask = isChildTask;

            formUploadData.TaskOpenStatus = string.IsNullOrEmpty(task.TaskOpenStatus)
                    ? new TaskOpenStatusModel()
                    : XmlHelper.Deserialize(typeof(TaskOpenStatusModel), task.TaskOpenStatus, Encoding.Unicode) as
                        TaskOpenStatusModel; //Know y and how

            formUploadData.TaskGuid = task.TaskInstanceId;
            formUploadData.taskxrefId = taskInstanceId;
            formUploadData.Concurrency = _taskManager.GetConcurrencyTimeStamp(task.TaskInstanceId);
            formUploadData.SequenceId = task.SequenceId;
            formUploadData.AssignedBy = task.AssignedBy;
            formUploadData.AssignedTo = task.AssignedTo;
            formUploadData.IsReassigned = task.IsReAssigned;
            formUploadData.TaskId = task.TaskId;
            formUploadData.PropertyId = _projectActionFormManager.GetProdPropertyInfo(formUploadData.FhaNumber).PropertyId;
            formUploadData.TaskStepId = task.TaskStepId;
            formUploadData.IsEditMode = isEditMode;
            formUploadData.pageTypeId =(int) task.PageTypeId;
            hasTaskOpenByUser = formUploadData.TaskOpenStatus.HasTaskOpenByUser(UserPrincipal.Current.UserName);

            // save task opened in task db
            //if (formUploadData.TaskOpenStatus != null)
            //{
            //    formUploadData.TaskOpenStatus.AddTaskOpenByUser(UserPrincipal.Current.UserName);
            //    task.TaskOpenStatus = XmlHelper.Serialize(formUploadData.TaskOpenStatus,
            //        typeof(TaskOpenStatusModel), Encoding.Unicode);
            //    _taskManager.UpdateTask(task);
            //}

            //Logic for reassign of parent task after a child request is created //Know y and how
            if (_parentChildTaskManager.IsParentTaskAvailable(task.TaskInstanceId))
            {
                var parenttask = _parentChildTaskManager.GetParentTask(task.TaskInstanceId);

                //if (parenttask != null)
                //{
                //    var newreassignedbyAe = string.Empty;

                //    newreassignedbyAe = taskReAssignmentManager.GetReassignedAE(parenttask.ParentTaskInstanceId);
                //    if (newreassignedbyAe != string.Empty)
                //        formUploadData.AssignedBy = newreassignedbyAe;
                //}
            }
            //Flag to identify Pam Report
            if (Request.QueryString.Count>1)
            {
                if (Request.QueryString["IsFromPam"] == "LenderPam")
                {
                    formUploadData.IsLenderPAMReport = true;
                }
                else
                {
                    formUploadData.IsPAMReport = true;
                }
            }
            formUploadData.ServicerSubmissionDate =
              TimezoneManager.GetPreferredTimeFromUtc((DateTime)formUploadData.ServicerSubmissionDate);
            TempData["ApplicationRequestFormData"] = formUploadData;
            TempData["hasTaskOpenByUser"] = hasTaskOpenByUser;

            return RedirectToAction("GetApplicationFormDetail", "ProductionApplication", new { model = formUploadData });


        }
		public ActionResult GetForm290Task(Guid taskInstanceId)
		{
			List<TaskFileModel> olistTaskFileModel = new List<TaskFileModel>();

			// throw new Exception("hi");
			var task = _taskManager.GetLatestTaskByTaskInstanceId(taskInstanceId);
			//var groupTaskModel = new GroupTaskModel();
			try
			{//skumar-form290
				if (task != null)
				{
					ViewBag.Form290TaskInstanceId = task.TaskInstanceId;
					var getPropertyInfo = _projectActionFormManager.GetProdPropertyInfo(task.FHANumber);
					var form290Task = _productionQueueManager.GetForm290TaskBySingleTaskInstanceID(taskInstanceId);
					var opaTask = _projectActionFormManager.GetOPAByTaskInstanceId(form290Task.ClosingTaskInstanceID);
					opaTask.PropertyId = getPropertyInfo.PropertyId;
					opaTask.PropertyName = getPropertyInfo.PropertyName;
					opaTask.ProjectActionName = EnumType.GetEnumDescription(
												EnumType.Parse<ProdProjectTypes>(opaTask.ProjectActionTypeId.ToString()));

					opaTask.TaskStepId = task.TaskStepId;
					opaTask.pageTypeId = (int)task.PageTypeId;
					opaTask.PropertyAddress = new AddressModel()
					{
						AddressLine1 = getPropertyInfo.StreetAddress,
						City = getPropertyInfo.City,
						StateCode = getPropertyInfo.State,
						ZIP = getPropertyInfo.Zipcode
					};

					opaTask.GroupTaskInstanceId = taskInstanceId;//skumar-form290- assign taskinstanceid to grouptaskinstanceid
					opaTask.pageTypeId = task.PageTypeId.HasValue ? task.PageTypeId.Value : 0;//skumar-form290 - get pagetype
					if (opaTask.pageTypeId == 0)
					{
						string formNameConfiguration = ConfigurationManager.AppSettings["PAGEID_ZERO"];
						opaTask.pageTypeId = !string.IsNullOrEmpty(formNameConfiguration) ? Convert.ToInt32(formNameConfiguration) : 0;
					}
					olistTaskFileModel = _taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceId).ToList();
					if (olistTaskFileModel.Count() > 0)
						ViewBag.FilesUploaded = "FilesAlreadyUploaded";

					return View("~/Views/Production/Form290Request/Form290.cshtml", opaTask);
				}
			}
			catch (Exception e)
			{
				return HttpNotFound();
			}

			return HttpNotFound();
		}

		[HttpPost]
        public ActionResult GetForm290Task(FormCollection pFormCollection)
        {
            return RedirectToAction("Index", "ProductionMyTask");
        }


        public JsonResult GetUploadedDocuments(string sidx, string sord, int page, int rows, Guid taskInstanceID)
        {
            int pageIndex = Convert.ToInt32(page) - 1;
            int pageSize = rows;
            // string userName="";
            var uploadedFilesByInstanceID = taskInstanceID == Guid.Empty ? new List<TaskFileModel>() : _taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceID).ToList();
            //var uploadedFilesByInstanceID = taskInstanceID == Guid.Empty ? new List<TaskFileModel>() : new List<TaskFileModel>(); //taskManager.GetAllTaskFileByTaskInstanceId(taskInstanceID).ToList();
            var uploadedFiles = (from data in uploadedFilesByInstanceID 
                                 //.Where(m => m.FileType == uploadType)
                                 let userName = internalExternalTaskManger.GetUserNameById(data.CreatedBy) //skumar-form290 - Name
                                 //let userName = internalExternalTaskManger.GetUserInfoById(data.CreatedBy).UserName
                                 select new
                                 {
                                     TaskFileID = data.TaskFileId,
                                     TaskInstanceID = data.TaskInstanceId,
                                     data.FileType,
                                     userName = "",//internalExternalTaskManger.GetUserInfoById(data.CreatedBy).UserName,
                                     Name = userName,
                                     Role = data.RoleName,//RoleManager.GetUserRoles(userName).FirstOrDefault(),
                                     data.FileName,
                                     data.FileSize,
                                     data.UploadTime
                                 });
            int totalrecods = uploadedFiles.Count();
            var totalpages = (int)Math.Ceiling((float)totalrecods / (float)rows);
            var results = uploadedFiles.Skip(pageIndex * pageSize).Take(pageSize);



            var jsonData = new
            {
                total = totalpages,
                page,
                records = totalrecods,
                rows = results,

            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }


        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="FolderKey"></param>
        /// <param name="Model"></param>
        /// <returns></returns>
        public ActionResult Form290Upload(int pFolderKey, string pModel)
        {

            ViewBag.uploadurl = "/ProductionApplication/SaveDropzoneJsUploadedFiles";
            string taskxrefId = string.Empty;

            //ViewBag.redirecturl = "/ProductionMyTask/GetTaskDetail?taskInstanceId=";
            //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

            var folderuploadModel = new FolderUploadModel { FolderKey = pFolderKey, CreatedBy = UserPrincipal.Current.UserId, ParentModel = pModel };
            return View("~/Views/InternalExternalTask/_DragDropFileUpload_InternExternal.cshtml", folderuploadModel);
        }


        //public ActionResult Form290Upload(Guid taskInstanceID)
        //{

        //    ViewBag.uploadurl = "/ProductionApplication/SaveDropzoneJsUploadedFiles";
        //    string taskxrefId = string.Empty;

        //    //ViewBag.redirecturl = "/ProductionMyTask/GetTaskDetail?taskInstanceId=";
        //    //  OPAViewModel AppModel = mydict.ToObject<OPAViewModel>();

        //    var folderuploadModel = new FolderUploadModel { FolderKey = 1, CreatedBy = UserPrincipal.Current.UserId, ParentModel = "" };
        //    return View("~/Views/InternalExternalTask/_DragDropFileUpload_InternExternal.cshtml", folderuploadModel);
        //}

    }
}
