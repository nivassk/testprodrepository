﻿using HUDHealthcarePortal.BusinessService;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HUDHealthcarePortal.Helpers
{
    public static class ExcelHelper
    {
        public static string ToCsv<T>(this IEnumerable<T> items) where T : class
        {
            var csvBuilder = new StringBuilder();
            var properties = typeof(T).GetProperties();
            //Header row
            foreach (var prop in properties)
            {
                csvBuilder.Append(prop.Name.ToCsvValue() + ",");
            }
            csvBuilder.AppendLine("");//Add line break
            //Body
            foreach (T item in items)
            {
                string line = string.Join(",", properties.Select(p => p.GetValue(item, null).ToCsvValue()).ToArray());
                csvBuilder.AppendLine(line);
            }
            return csvBuilder.ToString();
        }
        /// <summary>
        /// Helper method for dealing with nulls and escape characters
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ToCsvValue<T>(this T item)
        {
            if (item == null) return "\"\"";
            if (item is string)
            {
                return string.Format("\"{0}\"", item.ToString().Replace("\"", "\\\""));
            }
            double dummy;
            if (double.TryParse(item.ToString(), out dummy))
            {
                return string.Format("{0}", item);
            }
            return string.Format("\"{0}\"", item);
        }
        
        /// <summary>
        /// use gridview to render excel, bind to all collection of type T, show sub columns by hiding gridview columns
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="httpContext"></param>
        /// <param name="excelMgr">dictionary within controls columns to show and order of showing</param>
        /// <param name="dataSource"></param>
        public static void ExportToExcel<T>(HttpContextBase httpContext, ExcelManager<T> excelMgr, IEnumerable<T> dataSource) where T : class
        {
            var grid = new GridView();
            var dictHeaderToShow = excelMgr.GetHeaderDict();
            foreach (var item in dictHeaderToShow)
            {
                BoundField column = new BoundField();
                column.DataField = item.Key;
                column.HeaderText = item.Value.FriendlyHeaderName;
                column.HeaderStyle.BackColor = ColorTranslator.FromHtml(@"#C1D4E6");
                column.HeaderStyle.Font.Bold = true;
                column.DataFormatString = item.Value.DataFormatString;
                column.HeaderStyle.HorizontalAlign = item.Value.HeaderHorizAlign;
                column.ItemStyle.HorizontalAlign = item.Value.ItemHorizAlign;
                grid.Columns.Add(column);
            }

            grid.DataSource = dataSource;
            grid.DataBind();     

            httpContext.Response.ClearContent();
            httpContext.Response.AddHeader("content-disposition", "attachment; filename=excel.xls");
            httpContext.Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            httpContext.Response.Write(sw.ToString());

            httpContext.Response.End();
        }

        public static byte[] GetExcelBytes<T>(ExcelManager<T> excelMgr, IEnumerable<T> dataSource) where T : class
        {
            var grid = new GridView();
            grid.AutoGenerateColumns = false;
            var dictHeaderToShow = excelMgr.GetHeaderDict();
            foreach (var item in dictHeaderToShow)
            {
                BoundField column = new BoundField();
                column.DataField = item.Key;
                column.HeaderText = item.Value.FriendlyHeaderName;
                column.HeaderStyle.BackColor = ColorTranslator.FromHtml(@"#C1D4E6");
                column.HeaderStyle.Font.Bold = true;
                column.DataFormatString = item.Value.DataFormatString;
                column.HeaderStyle.HorizontalAlign = item.Value.HeaderHorizAlign;
                column.ItemStyle.HorizontalAlign = item.Value.ItemHorizAlign;
                grid.Columns.Add(column);
            }

            grid.DataSource = dataSource;
            grid.DataBind();

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            byte[] byteArray = Encoding.UTF8.GetBytes(sw.ToString());
            return byteArray;
        }
    }
}