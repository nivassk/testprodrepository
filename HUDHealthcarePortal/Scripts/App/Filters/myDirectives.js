﻿portfolioSummaryModule.filter('dateToISO', function () {
    return function (input) {
        if (input == "" || input == null)
            return "";
        return new Date(input).toISOString();
    };
});