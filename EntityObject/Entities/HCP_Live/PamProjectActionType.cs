﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EntityObject.Entities.HCP_live
{
    public class PamProjectActionType
    {
        [Key]
        public int PamProjectActionId { get; set; }
        public string Name { get; set; }
    }
}
