﻿

using EntityObject.Entities.HCP_live.Programmability;
namespace EntityObject.Entities.HCP_live
{
    public class usp_HCP_Prod_GetOpaHistoryForRequestAdditionalInfo_Result : usp_HCP_GetOpaHistoryForRequestAdditionalInfo_Result
    {
        #region Production Application
        public string id { get; set; }
        public string folderNodeName { get; set; }
        public int level { get; set; }
        public string parent { get; set; }
        public bool isLeaf { get; set; }
        public bool expanded { get; set; }
        public bool loaded { get; set; }
        public string DocTypeID { get; set; }
        public int? FolderKey { get; set; }
        public string SubfolderSequence { get; set; }
        public int? FolderSortingNumber { get; set; }
        #endregion
    }
}
