﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EntityObject.Entities.HCP_live
{
    
    public class ProjectActionForm
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public Guid ProjectActionFormId { get; set;}
        public string FhaNumber { get; set;}
        public string PropertyName { get; set; }
        public DateTime? RequestDate { get; set;}
        public DateTime? ServicerSubmissionDate { get; set;}
        public DateTime? ProjectActionStartDate { get; set;}
        public int ProjectActionTypeId { get; set;}
        public int RequestStatus { get; set;}
        public int MyTaskId { get; set;}
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public string ServicerComments { get; set; }
        public string AEComments { get; set; }

    }
}
