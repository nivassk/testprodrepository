﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace EntityObject.Entities.HCP_live
{
    public class RequestAdditionalInfo
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid RequestAdditionalInfoId { get; set;}
        public Guid RequestInstanceId { get; set;}
        public Guid ParentFormId { get; set;}
        public string FhaNumber { get; set;}
        public int ProjectActionTypeId { get; set;}
        public int RequestStatus { get; set;}
        public string Comments { get; set; }
        public int MyTaskId { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }

    }
}
