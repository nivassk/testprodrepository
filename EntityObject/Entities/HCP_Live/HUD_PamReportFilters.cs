﻿namespace EntityObject.Entities.HCP_live
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("HUD_PamReportFilters")]
    public class HUD_PamReportFilters
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FilterID { get; set; }

        [StringLength(100)]
        public string ProjectName { get; set; }

        [StringLength(15)]
        public string FhaNumber { get; set; }

        [StringLength(100)]
        public string LoanType { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        [StringLength(50)]
        public string FhaNumberRequest { get; set; }

        [StringLength(50)]
        public string UserRole { get; set; }

        [StringLength(50)]
        public string CorporateCreditReview { get; set; }

        [StringLength(50)]
        public string PortfolioNumberAssignment { get; set; }

        [StringLength(50)]
        public string MasterLeaseNumberAssignment { get; set; }

        [StringLength(50)]
        public string Underwriter { get; set; }

        [StringLength(50)]
        public string Appraiser { get; set; }

        [StringLength(50)]
        public string Environmentalist { get; set; }

        [StringLength(50)]
        public string Title_Survey { get; set; }

        [StringLength(50)]
        public string ExecutedDocuments { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? Dateto { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedON { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime ModifiedON { get; set; }

		[StringLength(100)]
		public string ProjectStage { get; set; }
	}
}
