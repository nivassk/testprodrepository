﻿namespace EntityObject.Entities.HCP_task
{
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;
	using System.Data.Entity.Spatial;

	[Table("RepairCostEstimateType")]
	public partial class RepairCostEstimateType
	{
		[Key]
		public int RepairCostEstimateTypeId { get; set; }
		public string RepairCostEstimate { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ModifiedOn { get; set; }

	}
}
